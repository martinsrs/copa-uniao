package com.synit.copauniao.service;

import com.synit.copauniao.dao.AdminUserDAO;
import com.synit.copauniao.entity.AdminUser;
import com.synit.copauniao.exception.UserNotAllowedException;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class LoginService {

    private static Logger logger = Logger.getLogger(LoginService.class);

    @Autowired
    private AdminUserDAO adminUserDAO;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public AdminUser login(String email, String password) throws UserNotAllowedException {
        final String _methodName = "login(email, password)";

        LoggingUtility.logEntering(logger, _methodName, email, "****");
        AdminUser result = adminUserDAO.getByEmailAndActiveTrueAndAdminRoleTrue(email);

        if (result != null) {
            if (!passwordEncoder.matches(password, result.getPassword())) {
                LoggingUtility.logError(logger, _methodName, "Password not matches!");
                result = null;
                throw new UserNotAllowedException();
            }
        }

        return result;
    }

}
