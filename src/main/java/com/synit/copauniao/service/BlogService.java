package com.synit.copauniao.service;

import com.synit.copauniao.dao.CommentDAO;
import com.synit.copauniao.dao.PostDAO;
import com.synit.copauniao.entity.Comment;
import com.synit.copauniao.entity.Post;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BlogService implements Service<Post> {
    private static Logger logger = Logger.getLogger(BlogService.class);

    @Autowired
    private PostDAO postDAO;

    @Autowired
    private CommentDAO commentDAO;

    @Override
    public List<Post> list() {
        final long start = System.currentTimeMillis();
        final String _methodName = "list()";
        LoggingUtility.logEntering(logger, _methodName);

        List<Post> result = postDAO.findAllByOrderByPostDateDesc();
        for(Post post : result) {
            post.setComments(listComments(post.getIdPost()));
        }

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    public Page<Post> listAvailable(Pageable pageable) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listAvailable()";
        LoggingUtility.logEntering(logger, _methodName);

        Page<Post> result = postDAO.findAllByPublishTrueOrderByPostDateDesc(pageable);
        for(Post post : result) {
            post.setComments(listComments(post.getIdPost()));
        }

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    @Override
    public void delete(Post post) {
        final long start = System.currentTimeMillis();
        final String _methodName = "delete(post)";
        LoggingUtility.logEntering(logger, _methodName, post.toString());

        postDAO.delete(post);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
    }

    @Override
    public Post get(Long id) {
        final long start = System.currentTimeMillis();
        final String _methodName = "get(Long id)";
        LoggingUtility.logEntering(logger, _methodName, id.toString());

        Post result = postDAO.getOne(id);
        result.setComments(listComments(id));

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    @Override
    public Post save(Post post) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(post)";
        LoggingUtility.logEntering(logger, _methodName, post.toString());

        Post result = postDAO.save(post);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    private List<Comment> listComments(Long idPost) {
        List<Comment> comments = commentDAO.findAllByPostIdPostOrderByCommentDateDesc(idPost);
        return comments;
    }
}
