package com.synit.copauniao.service;

import com.synit.copauniao.dao.SubscriptionDAO;
import com.synit.copauniao.dvo.Result;
import com.synit.copauniao.entity.Category;
import com.synit.copauniao.entity.Season;
import com.synit.copauniao.entity.Stage;
import com.synit.copauniao.entity.Subscription;
import com.synit.copauniao.util.log.LoggingUtility;
import com.synit.copauniao.util.sort.SortScore;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class ResultsService {

    private static Logger logger = Logger.getLogger(ResultsService.class);

    @Autowired
    private SubscriptionDAO subscriptionDAO;

    @Autowired
    private AthleteService athleteService;
    @Autowired
    private SeasonService seasonService;
    @Autowired
    private StageService stageService;
    @Autowired
    private CategoryService categoryService;

    private static Long AVULSO = 0L;


    public List<Subscription> listBySeasonStageCategory(Long idSeason, Long idStage, Long idCategory) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listBySeasonStageCategory(idSeason, idStage, idCategory)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString(), idStage.toString());

        Season season = seasonService.get(idSeason);
        Stage stage = stageService.get(idStage);
        Category category = categoryService.get(idCategory);

        List<Subscription> result = subscriptionDAO.findAllBySeasonAndStageAndCategoryOrderByPositionAsc(season, stage, category);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    public List<Result> listBySeasonCategory(Long idSeason, Long idCategory) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listBySeasonCategory(idSeason, idCategory)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString(), idCategory.toString());

        Season season = seasonService.get(idSeason);
        Category category = categoryService.get(idCategory);

        List<Subscription> subscriptions = subscriptionDAO.findAllBySeasonAndCategoryAndStageShowResultsTrue(season, category);
        List<Result> results = filterAthlete(subscriptions);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return results;
    }


    public List<Result> listResultsBySeasonStageCategory(Long idSeason, Long idStage, Long idCategory) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listBySeasonStage(idSeason, idStage)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString(), idStage.toString());

        Season season = seasonService.get(idSeason);
        Stage stage = stageService.get(idStage);
        Category category = categoryService.get(idCategory);

        List<Subscription> subscriptions = subscriptionDAO.findAllBySeasonAndStageAndCategoryAndStageShowResultsTrue(season, stage, category);

        List<Result> results = filterAthlete(subscriptions);
        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return results;
    }

    private List<Result> filterAthlete(List<Subscription> subscriptions) {
        List<Result> results = new ArrayList<>();
        for (Subscription sub : subscriptions) {
            Result result = new Result(sub);

            Integer score = result.getScore();
            Boolean add = true;
            for (Result r : results) {
                if (r.getAthlete().getIdAthlete().equals(sub.getAthlete().getIdAthlete())) {
                    r.setScore(r.getScore() + score);
                    add = false;
                }
            }

            if (add) {
                results.add(result);
            }
        }
        Collections.sort(results, new SortScore());

        return results;
    }


    public List<Result> listTeamBySeason(Long idSeason) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listTeamBySeasonCategory(idSeason)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString());

        Season season = seasonService.get(idSeason);

        List<Subscription> subscriptions = subscriptionDAO.findAllBySeasonAndStageShowResultsTrueOrderByCategoryAsc(season);

        List<Result> results = new ArrayList<>();
        for (Subscription sub : subscriptions) {
            Result result = new Result(sub);

            Integer score = result.getScore();
            Boolean add = true;
            for (Result r : results) {

                if (r.getTeam().getIdTeam().equals(sub.getTeam().getIdTeam())) {
                    r.setScore(r.getScore() + score);
                    add = false;
                }
            }

            if (add) {
                if (!result.getTeam().getIdTeam().equals(AVULSO)) {
                    results.add(result);
                }
            }
        }
        Collections.sort(results, new SortScore());

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return results;
    }

    public List<Subscription> save(List<Subscription> results) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(results)";
        LoggingUtility.logEntering(logger, _methodName, results.toString());

        List<Subscription> result = subscriptionDAO.saveAll(results);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

}
