package com.synit.copauniao.service;

import com.synit.copauniao.dao.CategoryDAO;
import com.synit.copauniao.entity.Category;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class CategoryService implements Service<Category> {

    private static Logger logger = Logger.getLogger(CategoryService.class);

    @Autowired
    private CategoryDAO categoryDAO;

    public List<Category> list() {
        final String _methodName = "list()";
        LoggingUtility.logEntering(logger, _methodName);

        List<Category> listSeasons = categoryDAO.findAllByOrderByNameAsc();

        return listSeasons;

    }

    public void delete(Category category) {
        final String _methodName = "delete(Season)";
        LoggingUtility.logEntering(logger, _methodName, category.toString());

        categoryDAO.delete(category);
    }


    public Category get(Long idCategory) {
        final String _methodName = "get(idCategory)";
        LoggingUtility.logEntering(logger, _methodName, idCategory.toString());

        Category category = categoryDAO.getOne(idCategory);

        return category;
    }

    public Category save(Category category) {
        final String _methodName = "save(Season)";
        LoggingUtility.logEntering(logger, _methodName, category.toString());

        Category saved = categoryDAO.save(category);

        return saved;
    }


}
