package com.synit.copauniao.service;

import com.synit.copauniao.dao.AdminUserDAO;
import com.synit.copauniao.entity.AdminUser;
import com.synit.copauniao.exception.DuplicatedUserException;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProfileService {

    private static Logger logger = Logger.getLogger(ProfileService.class);

    @Autowired
    private AdminUserDAO adminUserDAO;
    @Autowired
    private PasswordEncoder passwordEncoder;


    public List<AdminUser> list() {
        final String _methodName = "list()";
        LoggingUtility.logEntering(logger, _methodName);

        List<AdminUser> result = adminUserDAO.findAllByOrderByNameAscEmailAscIdUserAsc();

        return result;
    }

    public void delete(AdminUser adminUser) {
        final String _methodName = "delete(adminUser)";
        LoggingUtility.logEntering(logger, _methodName, adminUser.toString());

        adminUserDAO.delete(adminUser);
    }

    public AdminUser get(Long id) {
        final String _methodName = "get(id)";
        LoggingUtility.logEntering(logger, _methodName, id.toString());

        AdminUser result = adminUserDAO.getOne(id);

        return result;
    }

    public AdminUser save(AdminUser adminUser) throws DuplicatedUserException {
        final String _methodName = "save(adminUser)";
        LoggingUtility.logEntering(logger, _methodName, adminUser.toString());

        AdminUser result = new AdminUser();
        if (adminUser.getIdUser() == null) {
            try {
                result = registerNewUserAccount(adminUser);
            } catch (DuplicatedUserException ex) {
                LoggingUtility.logError(logger, _methodName, ex.getMessage());
                throw ex;
            }
        } else  {
            AdminUser user = this.get(adminUser.getIdUser());
            if (!adminUser.getPassword().equals(user.getPassword())) {
                adminUser.setPassword(passwordEncoder.encode(adminUser.getPassword()));
            }
            result = adminUserDAO.save(adminUser);
        }

        return result;
    }


    private Boolean emailExist(String email) {

        AdminUser user = adminUserDAO.getByEmail(email);
        if (user != null) {
            return true;
        }

        return false;
    }

    private AdminUser registerNewUserAccount(AdminUser user) throws DuplicatedUserException {
        if (emailExist(user.getEmail())) {
            throw new DuplicatedUserException("There is an account with that email address: " + user.getEmail());
        }

        AdminUser adminUser = new AdminUser();
        adminUser.setName(user.getName());
        adminUser.setPassword(passwordEncoder.encode(user.getPassword()));
        adminUser.setEmail(user.getEmail());
        adminUser.setActive(false);
        adminUser.setAdminRole(false);

        return adminUserDAO.save(adminUser);
    }

}
