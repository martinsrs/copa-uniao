package com.synit.copauniao.service;

import com.synit.copauniao.entity.Category;
import com.synit.copauniao.util.log.LoggingUtility;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface Service<VO> {

    public List<VO> list();
    public void delete(VO vo);
    public VO get(Long id);
    public VO save(VO vo);

}
