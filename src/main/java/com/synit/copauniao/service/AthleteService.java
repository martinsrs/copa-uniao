package com.synit.copauniao.service;

import com.synit.copauniao.controller.AthleteController;
import com.synit.copauniao.dao.AthleteDAO;
import com.synit.copauniao.entity.Athlete;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AthleteService implements Service<Athlete> {
    private static Logger logger = Logger.getLogger(AthleteService.class);

    @Autowired
    private AthleteDAO athleteDAO;

    @Override
    public List<Athlete> list() {
        final long start = System.currentTimeMillis();
        final String _methodName = "list()";
        LoggingUtility.logEntering(logger, _methodName);

        List<Athlete> result = athleteDAO.findAllByOrderByNameAsc();

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    @Override
    public void delete(Athlete athlete) {
        final long start = System.currentTimeMillis();
        final String _methodName = "delete(athlete)";
        LoggingUtility.logEntering(logger, _methodName, athlete.toString());

        athleteDAO.delete(athlete);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
    }

    public List<Athlete> listByName(String name) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listByName(name)";
        LoggingUtility.logEntering(logger, _methodName, name);

        List<Athlete> result = athleteDAO.findAllByNameContainingIgnoreCaseOrTeamNameContainingIgnoreCaseOrderByNameAsc(name, name);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    @Override
    public Athlete get(Long id) {
        final long start = System.currentTimeMillis();
        final String _methodName = "get(id)";
        LoggingUtility.logEntering(logger, _methodName, id.toString());

        Athlete result = athleteDAO.getOne(id);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    @Override
    public Athlete save(Athlete athlete) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(athlete)";
        LoggingUtility.logEntering(logger, _methodName, athlete.toString());

        Athlete result = athleteDAO.save(athlete);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }
}
