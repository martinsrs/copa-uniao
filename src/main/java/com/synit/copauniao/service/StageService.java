package com.synit.copauniao.service;

import com.synit.copauniao.dao.StageDAO;
import com.synit.copauniao.entity.Stage;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class    StageService implements Service<Stage> {

    private static Logger logger = Logger.getLogger(StageService.class);

    @Autowired
    private StageDAO stageDAO;


    @Override
    public List<Stage> list() {
        final long start = System.currentTimeMillis();
        final String _methodName = "list()";
        LoggingUtility.logEntering(logger, _methodName);

        List<Stage> result = stageDAO.findAllByPublishTrueAndShowResultsFalseOrderByDateAsc();

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    public List<Stage> listBySeason(Long idSeason) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listBySeason(idSeason)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString());

        List<Stage> result = stageDAO.findAllBySeasonIdSeasonOrderByDateAscIdStageAsc(idSeason);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    @Override
    public void delete(Stage stage) {
        final long start = System.currentTimeMillis();
        final String _methodName = "delete(stage)";
        LoggingUtility.logEntering(logger, _methodName, stage.toString());

        stageDAO.delete(stage);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
    }

    @Override
    public Stage get(Long id) {
        final long start = System.currentTimeMillis();
        final String _methodName = "get(id)";
        LoggingUtility.logEntering(logger, _methodName, id.toString());

        Stage result = stageDAO.getOne(id);

        LoggingUtility.logTimedMessage(logger, _methodName, start);

        return result;
    }

    @Override
    public Stage save(Stage stage) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(stage)";
        LoggingUtility.logEntering(logger, _methodName, stage.toString());

        Stage result = stageDAO.save(stage);

        LoggingUtility.logTimedMessage(logger, _methodName, start);

        return result;
    }
}
