package com.synit.copauniao.service;

import com.synit.copauniao.dao.TeamDAO;
import com.synit.copauniao.entity.Team;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TeamService implements Service<Team> {

    private static Logger logger = Logger.getLogger(TeamService.class);

    @Autowired
    private TeamDAO teamDAO;

    @Override
    public List<Team> list() {
        final long start = System.currentTimeMillis();
        final String _methodName = "list()";
        LoggingUtility.logEntering(logger, _methodName);

        List<Team> result = teamDAO.findAllByOrderByNameAsc();

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    @Override
    public void delete(Team team) {
        final long start = System.currentTimeMillis();
        final String _methodName = "delete(team)";
        LoggingUtility.logEntering(logger, _methodName, team.toString());

        teamDAO.delete(team);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
    }

    @Override
    public Team get(Long id) {
        final long start = System.currentTimeMillis();
        final String _methodName = "get(id)";
        LoggingUtility.logEntering(logger, _methodName, id.toString());

        Team result = teamDAO.getOne(id);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    @Override
    public Team save(Team team) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(team)";
        LoggingUtility.logEntering(logger, _methodName, team.toString());

        Team result = teamDAO.save(team);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }
}
