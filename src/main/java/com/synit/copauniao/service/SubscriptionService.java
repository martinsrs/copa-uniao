package com.synit.copauniao.service;

import com.synit.copauniao.dao.SubscriptionDAO;
import com.synit.copauniao.entity.*;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class SubscriptionService implements Service<Subscription> {

    private static Logger logger = Logger.getLogger(SubscriptionService.class);

    @Autowired
    private SubscriptionDAO subscriptionDAO;

    @Autowired
    private AthleteService athleteService;
    @Autowired
    private SeasonService seasonService;
    @Autowired
    private StageService stageService;
    @Autowired
    private CategoryService categoryService;


    @Override
    public List<Subscription> list() {
        return null;
    }

    public List<Subscription> listBySeasonStage(Long idSeason, Long idStage) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listBySeasonStage(idSeason, idStage)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString(), idStage.toString());

        Season season = seasonService.get(idSeason);
        Stage stage = stageService.get(idStage);

        List<Subscription> result = subscriptionDAO.findAllBySeasonAndStageOrderByCategoryAscAthleteNameAsc(season, stage);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;

    }

    public List<Subscription> listByAthleteSeasonStage(Long idAthlete, Long idSeason, Long idStage) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listByAthleteSeasonStage(athlete, idSeason, idStage)";
        LoggingUtility.logEntering(logger, _methodName, idAthlete.toString(), idSeason.toString(), idStage.toString());

        Athlete athlete = athleteService.get(idAthlete);
        Season season = seasonService.get(idSeason);
        Stage stage = stageService.get(idStage);

        List<Subscription> result = subscriptionDAO.findAllByAthleteAndSeasonAndStageOrderByCategoryNameAsc(athlete, season, stage);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    public List<Subscription> listBySeasonStageCategory(Long idSeason, Long idStage, Long idCategory) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listBySeasonStageCategory(idSeason, idStage, idCategory)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString(), idStage.toString());


        Season season = seasonService.get(idSeason);
        Stage stage = stageService.get(idStage);
        Category category = categoryService.get(idCategory);

        List<Subscription> result = subscriptionDAO.findAllBySeasonAndStageAndCategoryOrderByCategoryAscAthleteNameAsc(season, stage, category);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return result;
    }

    @Transactional
    public void deleteByAthleteSeasonStage(Long idAthlete, Long idSeason, Long idStage) {
        final long start = System.currentTimeMillis();
        final String _methodName = "deleteByAthleteSeasonStage(athlete, idSeason, idStage)";
        LoggingUtility.logEntering(logger, _methodName, idAthlete.toString(), idSeason.toString(), idStage.toString());

        Athlete athlete = athleteService.get(idAthlete);
        Season season = seasonService.get(idSeason);
        Stage stage = stageService.get(idStage);

        subscriptionDAO.deleteAllByAthleteAndSeasonAndStage(athlete, season, stage);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
    }

    @Override
    public void delete(Subscription subscription) {

    }

    @Override
    public Subscription get(Long id) {
        return null;
    }

    @Override
    public Subscription save(Subscription subscription) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(subscription)";
        LoggingUtility.logEntering(logger, _methodName, subscription.toString());

        Subscription result = subscriptionDAO.save(subscription);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return subscription;
    }
}
