package com.synit.copauniao.service;

import com.synit.copauniao.dao.SeasonDAO;
import com.synit.copauniao.entity.Season;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SeasonService implements Service<Season> {

    private static Logger logger = Logger.getLogger(SeasonService.class);

    @Autowired
    private SeasonDAO seasonDAO;

    public List<Season> list() {
        final String _methodName = "list()";
        LoggingUtility.logEntering(logger, _methodName);

        List<Season> listSeasons = seasonDAO.findAllByOrderByYearAscEditionAscIdSeasonAsc();

        return listSeasons;
    }

    public List<Season> listActiveSeasons() {
        final String _methodName = "listActiveSeasons()";
        LoggingUtility.logEntering(logger, _methodName);

        List<Season> listSeasons = seasonDAO.findAllByActiveOrderByYearAscEditionAscIdSeasonAsc(true);

        return listSeasons;
    }

    public void delete(Season season) {
        final String _methodName = "delete(Season)";
        LoggingUtility.logEntering(logger, _methodName, season.toString());

        seasonDAO.delete(season);
    }


    public Season get(Long idSeason) {
        final String _methodName = "get(idSeason)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString());

        Season season = seasonDAO.getOne(idSeason);

        return season;
    }

    public Season save(Season season) {
        final String _methodName = "save(Season)";
        LoggingUtility.logEntering(logger, _methodName, season.toString());

        Season saved = seasonDAO.save(season);

        return saved;
    }

}
