package com.synit.copauniao.exception;

public class DuplicatedUserException extends Exception {

    @Override
    public String getMessage() {
        return "Duplicated User";
    }

    public DuplicatedUserException(String message) {
        super(message);
    }
}
