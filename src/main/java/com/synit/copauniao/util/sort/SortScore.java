package com.synit.copauniao.util.sort;

import com.synit.copauniao.dvo.Result;
import com.synit.copauniao.entity.Category;

import java.util.Comparator;

public class SortScore implements Comparator<Result> {

    public int compare(Result a, Result b){
        return b.getScore().compareTo(a.getScore());
    }


}
