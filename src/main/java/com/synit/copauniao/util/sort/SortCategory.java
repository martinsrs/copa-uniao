package com.synit.copauniao.util.sort;

import com.synit.copauniao.entity.Category;

import java.util.Comparator;

public class SortCategory implements Comparator<Category> {

    public int compare(Category a, Category b){
        return a.getName().compareTo(b.getName());
    }

}
