package com.synit.copauniao.util;

public class Score {

    public static Integer SCORE_1 = 18;
    public static Integer SCORE_2 = 15;
    public static Integer SCORE_3 = 12;
    public static Integer SCORE_4 = 10;
    public static Integer SCORE_5 = 9;
    public static Integer SCORE_6 = 8;
    public static Integer SCORE_7 = 7;
    public static Integer SCORE_8 = 6;
    public static Integer SCORE_9 = 5;
    public static Integer SCORE_10 = 4;
    public static Integer SCORE_11 = 2;
    public static Integer SCORE_NC = 1;


}
