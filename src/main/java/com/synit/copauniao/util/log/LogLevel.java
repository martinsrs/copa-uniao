package com.synit.copauniao.util.log;

public enum LogLevel {

    INFO, DEBUG, TRACE, ERROR;

}
