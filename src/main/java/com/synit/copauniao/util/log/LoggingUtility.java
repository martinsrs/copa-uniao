package com.synit.copauniao.util.log;

import org.apache.log4j.Logger;

public class LoggingUtility {

    public LoggingUtility() {
    }

    private static double secondsElapsed(long start) {
        return (((double) System.currentTimeMillis() - (double) start) / (double) 1000.00);
    }

    private static Logger getClassLogger(String cls) {
        try {
            Class<?> clazz = Class.forName(cls);
            return Logger.getLogger(clazz);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void logEntering(Logger logger, String methodName) {
        logEntering(logger,methodName, null);
    }

    public static void logEntering(Logger logger, String methodName, String... params) {
        Logger _logger = getClassLogger(logger.getName());
        String parameters = "";
        if (params != null) {
            parameters = "params[";
            for (String _p : params) {
                parameters = parameters + _p + ",";
            }
            parameters = parameters.substring(0, parameters.length() - 1) + "]";
        }
        _logger.info(getMethodEntering(logger.getName(),methodName, parameters));
    }

    public static void logMessage(Logger logger, LogLevel logLevel, String methodName, String msg) {
        Logger _logger = getClassLogger(logger.getName());

        switch (logLevel) {
            case INFO:
                _logger.info(getFormattedLogMessage(logger.getName(),methodName,msg));
                break;
            case DEBUG:
                if (_logger.isDebugEnabled()) {
                    _logger.debug(getFormattedLogMessage(logger.getName(),methodName,msg));
                }
                break;
            case TRACE:
                if (_logger.isTraceEnabled()) {
                    _logger.trace(getFormattedLogMessage(logger.getName(),methodName,msg));
                }
                break;
            case ERROR:
                _logger.error(getFormattedLogMessage(logger.getName(),methodName,msg));
                break;
        }
    }

    public static void logInfo(Logger logger, String methodName, String msg) {
        logMessage(logger, LogLevel.INFO, methodName, msg);
    }
    public static void logDebug(Logger logger, String methodName, String msg) {
        logMessage(logger, LogLevel.DEBUG, methodName, msg);
    }
    public static void logTrace(Logger logger, String methodName, String msg) {
        logMessage(logger, LogLevel.TRACE, methodName, msg);
    }
    public static void logError(Logger logger, String methodName, String msg) {
        logMessage(logger, LogLevel.ERROR, methodName, msg);
    }

    public static void logTimedMessage(Logger logger, String methodName, long startTime) {
        logMessage(logger, LogLevel.INFO, methodName, getTimedLogMessage(startTime));
    }

    public static String getMethodEntering(String className, String methodName) {
        return getMethodEntering(className, methodName, null);
    }

    public static String getMethodEntering(String className, String methodName, String params) {
        StringBuilder sb = new StringBuilder(256);
        sb.append(className).append('.').append(methodName).append(": Entering. ");

        if (params != null) {
            sb.append(params);
        }

        return sb.toString();
    }

    public static String getMethodReturning(String className, String methodName) {
        return getMethodEntering(className, methodName, null);
    }

    public static String getMethodReturning(String className, String methodName, String msg) {
        StringBuilder sb = new StringBuilder(256);
        sb.append(className).append('.').append(methodName).append(": Returning. ");

        if (msg != null) {
            sb.append(msg);
        }

        return sb.toString();
    }

    public static String getFormattedLogMessage(String className, String methodName) {
        return getFormattedLogMessage(className, methodName, null);
    }

    public static String getFormattedLogMessage(String className, String methodName, String msg) {
        StringBuilder sb = new StringBuilder(256);
        sb.append(className).append('.').append(methodName);
        if (msg != null) {
            sb.append(": ").append(msg);
        }

        return sb.toString();
    }

    private static String getTimedLogMessage(long startTime) {
        return getTimedLogMessage("Call Time", startTime);
    }

    private static String getTimedLogMessage(String msg, long startTime) {
        double elapsedSeconds = secondsElapsed(startTime);
        StringBuilder sb = new StringBuilder(msg);
        sb.append(" - [").append(elapsedSeconds).append("] seconds");

        return sb.toString();
    }
}