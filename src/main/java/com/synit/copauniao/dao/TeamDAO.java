package com.synit.copauniao.dao;

import com.synit.copauniao.entity.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamDAO extends JpaRepository<Team, Long> {

    List<Team> findAllByOrderByNameAsc();

}
