package com.synit.copauniao.dao;

import com.synit.copauniao.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostDAO extends JpaRepository<Post, Long> {

    List<Post> findAllByOrderByPostDateDesc();
    Page<Post> findAllByPublishTrueOrderByPostDateDesc(Pageable pageable);

}
