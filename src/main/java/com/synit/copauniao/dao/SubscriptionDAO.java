package com.synit.copauniao.dao;

import com.synit.copauniao.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubscriptionDAO extends JpaRepository<Subscription, Long> {


    List<Subscription> findAllByAthleteAndSeasonIdSeasonAndStageIdStageOrderByCategoryNameAsc(Athlete athlete, Long idSeason, Long idStage);
    List<Subscription> findAllByAthleteAndSeasonAndStageOrderByCategoryNameAsc(Athlete athlete, Season season, Stage Stage);
    List<Subscription> findAllBySeasonAndStageOrderByCategoryAscAthleteNameAsc(Season season, Stage stage);
    List<Subscription> findAllBySeasonAndStageAndCategoryOrderByCategoryAscAthleteNameAsc(Season season, Stage stage, Category category);

    List<Subscription> findAllBySeasonAndCategoryAndStageShowResultsTrue(Season season, Category category);
    List<Subscription> findAllBySeasonAndStageShowResultsTrueOrderByCategoryAsc(Season season);

    List<Subscription> findAllBySeasonAndStageAndCategoryAndStageShowResultsTrue(Season season, Stage stage, Category category);

    List<Subscription> findAllBySeasonAndStageAndCategoryOrderByPositionAsc(Season season, Stage stage, Category category);

    void deleteAllByAthleteAndSeasonAndStage(Athlete athlete, Season season, Stage Stage);


}
