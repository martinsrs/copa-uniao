package com.synit.copauniao.dao;

import com.synit.copauniao.entity.AdminUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AdminUserDAO extends JpaRepository<AdminUser, Long> {


    List<AdminUser> findAllByOrderByNameAscEmailAscIdUserAsc();
    AdminUser getByEmailAndPasswordAndActiveTrue(String email, String password);
    AdminUser getByEmail(String email);
    AdminUser getByEmailAndActiveTrueAndAdminRoleTrue(String email);
    AdminUser getByEmailAndPasswordAndActiveTrueAndAdminRoleTrue(String email, String password);


}
