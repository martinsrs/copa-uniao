package com.synit.copauniao.dao;

import com.synit.copauniao.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentDAO extends JpaRepository<Comment, Long> {

    List<Comment> findAllByPostIdPostOrderByCommentDateDesc(Long idPost);

}
