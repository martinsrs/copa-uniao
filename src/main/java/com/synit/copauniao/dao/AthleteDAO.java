package com.synit.copauniao.dao;

import com.synit.copauniao.entity.Athlete;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AthleteDAO extends JpaRepository<Athlete, Long> {

    List<Athlete> findAllByOrderByNameAsc();
    List<Athlete> findAllByNameContainingIgnoreCaseOrTeamNameContainingIgnoreCaseOrderByNameAsc(String name, String teamName);

}
