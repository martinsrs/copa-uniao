package com.synit.copauniao.dao;

import com.synit.copauniao.entity.Season;
import com.synit.copauniao.entity.Stage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StageDAO extends JpaRepository<Stage, Long> {

    List<Stage> findAllByPublishTrueAndShowResultsFalseOrderByDateAsc();
    List<Stage> findAllBySeasonIdSeasonOrderByDateAscIdStageAsc(Long idSeason);


}
