package com.synit.copauniao.dao;


import com.synit.copauniao.entity.Season;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SeasonDAO extends JpaRepository<Season, Long> {


    List<Season> findAllByOrderByYearAscEditionAscIdSeasonAsc();
    List<Season> findAllByActiveOrderByYearAscEditionAscIdSeasonAsc(Boolean active);


}
