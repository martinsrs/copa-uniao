package com.synit.copauniao.dvo;

import com.synit.copauniao.entity.Athlete;
import com.synit.copauniao.entity.Season;
import com.synit.copauniao.entity.Stage;
import com.synit.copauniao.entity.Team;

import java.util.List;

public class SubscriptionVO {

    private Athlete athlete;
    private Team team;
    private Season season;
    private Stage stage;
    private List<CategorySubscription> categories;

    public Athlete getAthlete() {
        return athlete;
    }

    public void setAthlete(Athlete athlete) {
        this.athlete = athlete;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public List<CategorySubscription> getCategories() {
        return categories;
    }

    public void setCategories(List<CategorySubscription> categories) {
        this.categories = categories;
    }
}
