package com.synit.copauniao.dvo;

import com.synit.copauniao.entity.*;
import com.synit.copauniao.util.Score;

public class Result {

    private Category category;
    private Athlete athlete;
    private Team team;
    private Stage stage;
    private Long position;
    private Integer score;

    public Result(){

    }

    public Result(Subscription subscription) {
        this.setAthlete(subscription.getAthlete());
        this.setCategory(subscription.getCategory());
        this.setTeam(subscription.getTeam());
        this.setStage(subscription.getStage());
        this.setPosition(subscription.getPosition());

        this.setScore(this.calculateScore());
    }


    public Integer calculateScore() {

        Integer score = 0;

        if (this.getPosition() != null) {
            if (this.getPosition().compareTo(10L) == 1) {
                score = Score.SCORE_11;
            }
            if (this.getPosition().compareTo(999L) == 0) {
                score = Score.SCORE_NC;
            }

            switch (this.getPosition().intValue()) {
                case 1 : score = Score.SCORE_1;
                    break;
                case 2 : score = Score.SCORE_2;
                    break;
                case 3 : score = Score.SCORE_3;
                    break;
                case 4 : score = Score.SCORE_4;
                    break;
                case 5 : score = Score.SCORE_5;
                    break;
                case 6 : score = Score.SCORE_6;
                    break;
                case 7 : score = Score.SCORE_7;
                    break;
                case 8 : score = Score.SCORE_8;
                    break;
                case 9 : score = Score.SCORE_9;
                    break;
                case 10 : score = Score.SCORE_10;
                    break;
            }
        }



        return score;
    }


    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Athlete getAthlete() {
        return athlete;
    }

    public void setAthlete(Athlete athlete) {
        this.athlete = athlete;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
