package com.synit.copauniao.dvo;

import com.synit.copauniao.entity.Category;

public class CategorySubscription extends Category {

    private Long number;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }
}
