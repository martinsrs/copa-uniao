package com.synit.copauniao.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface Controller<VO> {

    public ResponseEntity<List<VO>> list();
    public ResponseEntity<VO> save(VO vo);
    public ResponseEntity delete(Long id);

}
