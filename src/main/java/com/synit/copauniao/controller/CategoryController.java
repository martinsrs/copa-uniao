package com.synit.copauniao.controller;

import com.synit.copauniao.entity.Category;
import com.synit.copauniao.entity.Season;
import com.synit.copauniao.service.CategoryService;
import com.synit.copauniao.service.SeasonService;
import com.synit.copauniao.util.log.LoggingUtility;
import com.synit.copauniao.util.sort.SortCategory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RestController
public class CategoryController extends BaseController implements Controller<Category>{

    private static Logger logger = Logger.getLogger(CategoryController.class);

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SeasonService seasonService;

    @GetMapping("category/")
    public ResponseEntity<List<Category>> list() {
        final long start = System.currentTimeMillis();
        final String _methodName = "list()";
        LoggingUtility.logEntering(logger, _methodName);

        List<Category> result = categoryService.list();

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @GetMapping("category/season/{idSeason}")
    public ResponseEntity<List<Category>> listBySeason(@PathVariable Long idSeason) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listBySeason(idSeason)";
        LoggingUtility.logEntering(logger, _methodName);

        Season season = seasonService.get(idSeason);
        List<Category> result = season.getCategories();

        Collections.sort(season.getCategories(), new SortCategory());

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @GetMapping("category/{id}")
    public ResponseEntity<Category> get(@PathVariable Long id) {
        final long start = System.currentTimeMillis();
        final String _methodName = "list(id)";
        LoggingUtility.logEntering(logger, _methodName, id.toString());

        Category result = categoryService.get(id);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("category/")
    public ResponseEntity<Category> save(@RequestBody Category category) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save()";
        LoggingUtility.logEntering(logger, _methodName, category.toString());

        Category result = categoryService.save(category);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("category/delete/{idCategory}")
    public ResponseEntity delete(@PathVariable Long idCategory) {
        final long start = System.currentTimeMillis();
        final String _methodName = "delete(idCategory)";
        LoggingUtility.logEntering(logger, _methodName, idCategory.toString());

        Category category = categoryService.get(idCategory);
        if (category != null) {
            categoryService.delete(category);
        }

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok("");
    }

}
