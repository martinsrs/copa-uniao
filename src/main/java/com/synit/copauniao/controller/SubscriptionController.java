package com.synit.copauniao.controller;

import com.synit.copauniao.dvo.CategorySubscription;
import com.synit.copauniao.dvo.SubscriptionVO;
import com.synit.copauniao.entity.Subscription;
import com.synit.copauniao.service.SubscriptionService;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class SubscriptionController extends BaseController {

    private static Logger logger = Logger.getLogger(SubscriptionController.class);

    @Autowired
    private SubscriptionService subscriptionService;

    @GetMapping("subscription/athlete/{idAthlete}/season/{idSeason}/stage/{idStage}")
    public ResponseEntity<List<Subscription>> listByAthleteSeasonStage(@PathVariable Long idAthlete, @PathVariable Long idSeason, @PathVariable Long idStage) {

        final long start = System.currentTimeMillis();
        final String _methodName = "listByAthleteSeasonStage(idAthlete,idSeason,idStage)";
        LoggingUtility.logEntering(logger, _methodName, idAthlete.toString(), idSeason.toString(), idStage.toString());

        List<Subscription> result = subscriptionService.listByAthleteSeasonStage(idAthlete, idSeason, idStage);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return  ResponseEntity.ok(result);
    }

    @GetMapping("subscription/season/{idSeason}/stage/{idStage}")
    public ResponseEntity<List<Subscription>> listBySeasonStage(@PathVariable Long idSeason, @PathVariable Long idStage) {

        final long start = System.currentTimeMillis();
        final String _methodName = "listBySeasonStage(idSeason,idStage)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString(), idStage.toString());

        List<Subscription> result = subscriptionService.listBySeasonStage(idSeason, idStage);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return  ResponseEntity.ok(result);
    }

    @GetMapping("subscription/season/{idSeason}/stage/{idStage}/category/{idCategory}")
    public ResponseEntity<List<Subscription>> listBySeasonStageCategory(@PathVariable Long idSeason, @PathVariable Long idStage, @PathVariable Long idCategory) {

        final long start = System.currentTimeMillis();
        final String _methodName = "listBySeasonStageCategory(idSeason,idStage,idCategory)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString(), idStage.toString(), idCategory.toString());

        List<Subscription> result = subscriptionService.listBySeasonStageCategory(idSeason, idStage, idCategory);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return  ResponseEntity.ok(result);
    }

    @PostMapping("subscription/")
    public ResponseEntity<List<Subscription>> save(@RequestBody SubscriptionVO subscriptionVO) {

        final long start = System.currentTimeMillis();
        final String _methodName = "save(subscriptionVO)";
        LoggingUtility.logEntering(logger, _methodName, subscriptionVO.toString());

        subscriptionService.deleteByAthleteSeasonStage(subscriptionVO.getAthlete().getIdAthlete(), subscriptionVO.getSeason().getIdSeason(), subscriptionVO.getStage().getIdStage());

        List<Subscription> result = new ArrayList<>();
        if (subscriptionVO.getCategories() != null) {
            for (CategorySubscription category : subscriptionVO.getCategories()) {
                Subscription subscription = new Subscription();
                subscription.setAthlete(subscriptionVO.getAthlete());
                subscription.setTeam(subscriptionVO.getTeam());
                subscription.setCategory(category);
                subscription.setNumber(category.getNumber());
                subscription.setSeason(subscriptionVO.getSeason());
                subscription.setStage(subscriptionVO.getStage());

                Subscription oneSubs = subscriptionService.save(subscription);

                result.add(oneSubs);
            }
        }

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    public ResponseEntity delete(Long id) {
        return null;
    }
}
