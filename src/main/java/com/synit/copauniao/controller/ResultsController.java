package com.synit.copauniao.controller;

import com.synit.copauniao.dvo.Result;
import com.synit.copauniao.entity.Subscription;
import com.synit.copauniao.service.ResultsService;
import com.synit.copauniao.service.SubscriptionService;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ResultsController extends BaseController {

    private static Logger logger = Logger.getLogger(ResultsController.class);

    @Autowired
    private ResultsService resultsService;

    @GetMapping("results/season/{idSeason}/stage/{idStage}/category/{idCategory}")
    public ResponseEntity<List<Subscription>> listBySeasonStageCategory(@PathVariable Long idSeason, @PathVariable Long idStage, @PathVariable Long idCategory) {

        final long start = System.currentTimeMillis();
        final String _methodName = "listBySeasonStageCategory(idAthlete,idSeason,idStage, idCategory)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString(), idStage.toString(), idCategory.toString());

        List<Subscription> result = resultsService.listBySeasonStageCategory(idSeason, idStage, idCategory);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return  ResponseEntity.ok(result);
    }

    @GetMapping("results/season/{idSeason}/category/{idCategory}")
    public ResponseEntity<List<Result>> listResultsBySeasonCategory(@PathVariable Long idSeason, @PathVariable Long idCategory) {

        final long start = System.currentTimeMillis();
        final String _methodName = "listResultsBySeasonCategory(idSeason, idCategory)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString(), idCategory.toString());

        List<Result> result = resultsService.listBySeasonCategory(idSeason, idCategory);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return  ResponseEntity.ok(result);
    }

    @GetMapping("results/season/{idSeason}/stage/{idStage}/category/{idCategory}/table")
    public ResponseEntity<List<Result>> listResultsBySeasonStageCategory(@PathVariable Long idSeason, @PathVariable Long idStage, @PathVariable Long idCategory) {

        final long start = System.currentTimeMillis();
        final String _methodName = "listResultsBySeasonStageCategory(idSeason, idStage, idCategory)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString(), idStage.toString(), idCategory.toString());

        List<Result> result = resultsService.listResultsBySeasonStageCategory(idSeason, idStage, idCategory);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return  ResponseEntity.ok(result);
    }

    @GetMapping("results/team/season/{idSeason}")
    public ResponseEntity<List<Result>> listTeamResultsBySeason(@PathVariable Long idSeason) {

        final long start = System.currentTimeMillis();
        final String _methodName = "listTeamResults(idSeason)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString());

        List<Result> result = resultsService.listTeamBySeason(idSeason);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return  ResponseEntity.ok(result);
    }


    @PostMapping("results/")
    public ResponseEntity<List<Subscription>> save(@RequestBody List<Subscription> results) {

        final long start = System.currentTimeMillis();
        final String _methodName = "save(results)";
        LoggingUtility.logEntering(logger, _methodName, results.toString());

        List<Subscription> result = resultsService.save(results);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return  ResponseEntity.ok(result);
    }
}
