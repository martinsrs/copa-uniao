package com.synit.copauniao.controller;

import com.synit.copauniao.entity.AdminUser;
import com.synit.copauniao.exception.DuplicatedUserException;
import com.synit.copauniao.service.ProfileService;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserProfileController extends BaseController {

    private static Logger logger = Logger.getLogger(LoginController.class);

    @Autowired
    private ProfileService profileService;

    @GetMapping("profile/")
    public ResponseEntity<List<AdminUser>> list() {
        final long start = System.currentTimeMillis();
        final String _methodName = "list()";
        LoggingUtility.logEntering(logger, _methodName);

        List<AdminUser> result = profileService.list();

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @GetMapping("profile/{id}")
    public ResponseEntity<AdminUser> get(@PathVariable Long id) {
        final long start = System.currentTimeMillis();
        final String _methodName = "get(id)";
        LoggingUtility.logEntering(logger, _methodName, id.toString());

        AdminUser result = profileService.get(id);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("profile/")
    public ResponseEntity<AdminUser> save(@RequestBody AdminUser adminUser) throws DuplicatedUserException {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(adminUser)";
        LoggingUtility.logEntering(logger, _methodName, adminUser.toString());


        if (adminUser.getIdUser() == null) {
            if (adminUser.getAdminRole() == null) {
                adminUser.setAdminRole(false);
            }
            if (adminUser.getActive() == null) {
                adminUser.setActive(false);
            }
        }

        AdminUser result = profileService.save(adminUser);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("profile/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        final long start = System.currentTimeMillis();
        final String _methodName = "delete(id)";
        LoggingUtility.logEntering(logger, _methodName, id.toString());

        AdminUser adminUser = profileService.get(id);
        profileService.delete(adminUser);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok("");
    }

}
