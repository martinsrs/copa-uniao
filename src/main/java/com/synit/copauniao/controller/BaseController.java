package com.synit.copauniao.controller;

import com.synit.copauniao.exception.DuplicatedUserException;
import com.synit.copauniao.exception.UserNotAllowedException;
import com.synit.copauniao.exception.UserNotFoundException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("services/v1")
public class BaseController {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({UserNotFoundException.class, EmptyResultDataAccessException.class})
    public void handleUserNotFound() {

    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(UserNotAllowedException.class)
    public void handleUserNotAllowed() {

    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(DuplicatedUserException.class)
    public void handleConflict() {

    }

}
