package com.synit.copauniao.controller;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.synit.copauniao.entity.Category;
import com.synit.copauniao.entity.Team;
import com.synit.copauniao.service.TeamService;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TeamController extends BaseController implements Controller<Team> {

    private static Logger logger = Logger.getLogger(TeamController.class);

    @Autowired
    private TeamService teamService;


    @GetMapping("team/")
    @Override
    public ResponseEntity<List<Team>> list() {
        final long start = System.currentTimeMillis();
        final String _methodName = "list()";
        LoggingUtility.logEntering(logger, _methodName);

        List<Team> result = teamService.list();

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("team/")
    @Override
    public ResponseEntity<Team> save(@RequestBody Team team) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(team)";
        LoggingUtility.logEntering(logger, _methodName, team.toString());

        Team result = teamService.save(team);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("team/delete/{idTeam}")
    @Override
    public ResponseEntity delete(@PathVariable Long idTeam) {
        final long start = System.currentTimeMillis();
        final String _methodName = "delete(idTeam)";
        LoggingUtility.logEntering(logger, _methodName, idTeam.toString());

        Team team = teamService.get(idTeam);
        if (team != null) {
            teamService.delete(team);
        }

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok("");
    }
}
