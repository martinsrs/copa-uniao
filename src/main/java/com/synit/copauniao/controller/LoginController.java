package com.synit.copauniao.controller;

import com.synit.copauniao.dvo.LoginUser;
import com.synit.copauniao.entity.AdminUser;
import com.synit.copauniao.exception.UserNotAllowedException;
import com.synit.copauniao.service.LoginService;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController extends BaseController {

    private static Logger logger = Logger.getLogger(LoginController.class);

    @Autowired
    private LoginService loginService;

    @PostMapping("login/")
    public ResponseEntity<AdminUser> login(@RequestBody LoginUser user) throws UserNotAllowedException{

        final long start = System.currentTimeMillis();
        final String _methodName = "login(user)";
        LoggingUtility.logEntering(logger, _methodName, user.toString());

        AdminUser result = loginService.login(user.getEmail(), user.getPassword());

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return  ResponseEntity.ok(result);
    }


}
