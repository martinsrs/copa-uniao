package com.synit.copauniao.controller;

import com.synit.copauniao.entity.Category;
import com.synit.copauniao.entity.Season;
import com.synit.copauniao.service.SeasonService;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SeasonController extends BaseController implements Controller<Season> {

    private static Logger logger = Logger.getLogger(SeasonController.class);

    @Autowired
    private SeasonService seasonService;

    @GetMapping("season/")
    public ResponseEntity<List<Season>> list() {
        final long start = System.currentTimeMillis();
        final String _methodName = "list()";

        LoggingUtility.logEntering(logger, _methodName);

        List<Season> result = seasonService.list();

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @GetMapping("season/active")
    public ResponseEntity<List<Season>> listActiveSeasons() {
        final long start = System.currentTimeMillis();
        final String _methodName = "listActiveSeasons()";

        LoggingUtility.logEntering(logger, _methodName);

        List<Season> result = seasonService.listActiveSeasons();

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @GetMapping("season/{idSeason}")
    public ResponseEntity<Season> get(@PathVariable Long idSeason) {
        final long start = System.currentTimeMillis();
        final String _methodName = "get(idSeason)";

        LoggingUtility.logEntering(logger, _methodName, idSeason.toString());

        Season result = seasonService.get(idSeason);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("season/")
    public ResponseEntity<Season> save(@RequestBody Season season) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save()";
        LoggingUtility.logEntering(logger, _methodName, season.toString());

        Season result = seasonService.save(season);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("season/delete/{idSeason}")
    public ResponseEntity delete(@PathVariable Long idSeason) {
        final long start = System.currentTimeMillis();
        final String _methodName = "delete(idSeason)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString());

        Season season = seasonService.get(idSeason);
        if (season != null) {
            seasonService.delete(season);
        }

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok("");
    }

    @PostMapping("season/{idSeason}/category/")
    public ResponseEntity<Season> assignCategories(@PathVariable Long idSeason, @RequestBody List<Category> categories) {
        final long start = System.currentTimeMillis();
        final String _methodName = "assignCategories(idSeason, categories)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString(), categories.toString());

        Season update = seasonService.get(idSeason);
        update.setCategories(categories);

        Season saved = seasonService.save(update);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(saved);
    }

}
