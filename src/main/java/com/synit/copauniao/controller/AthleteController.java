package com.synit.copauniao.controller;

import com.synit.copauniao.entity.Athlete;
import com.synit.copauniao.service.AthleteService;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AthleteController extends BaseController implements Controller<Athlete> {

    private static Logger logger = Logger.getLogger(AthleteController.class);

    @Autowired
    private AthleteService athleteService;

    @GetMapping("athlete/")
    @Override
    public ResponseEntity<List<Athlete>> list() {
        final long start = System.currentTimeMillis();
        final String _methodName = "list()";
        LoggingUtility.logEntering(logger, _methodName);

        List<Athlete> result = athleteService.list();

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @GetMapping("athlete/{name}")
    public ResponseEntity<List<Athlete>> listByName(@PathVariable String name) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listByName(name)";
        LoggingUtility.logEntering(logger, _methodName, name);

        List<Athlete> result = athleteService.listByName(name);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("athlete/")
    @Override
    public ResponseEntity<Athlete> save(@RequestBody Athlete athlete) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(athlete)";
        LoggingUtility.logEntering(logger, _methodName, athlete.toString());

        Athlete result = athleteService.save(athlete);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("athlete/delete/{id}")
    @Override
    public ResponseEntity delete(@PathVariable Long id) {
        final long start = System.currentTimeMillis();
        final String _methodName = "delete(idAthlete)";
        LoggingUtility.logEntering(logger, _methodName, id.toString());

        Athlete athlete = athleteService.get(id);
        if (athlete != null) {
            athleteService.delete(athlete);
        }

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok("");
    }
}
