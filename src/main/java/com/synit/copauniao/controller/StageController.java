package com.synit.copauniao.controller;

import com.synit.copauniao.entity.Category;
import com.synit.copauniao.entity.Stage;
import com.synit.copauniao.service.CategoryService;
import com.synit.copauniao.service.StageService;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Path;
import java.util.List;

@RestController
public class StageController extends BaseController implements Controller<Stage>{

    private static Logger logger = Logger.getLogger(StageController.class);

    @Autowired
    private StageService stageService;

    @GetMapping("stage/")
    @Override
    public ResponseEntity<List<Stage>> list() {
        final long start = System.currentTimeMillis();
        final String _methodName = "list()";
        LoggingUtility.logEntering(logger, _methodName);

        List<Stage> result = stageService.list();

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @GetMapping("stage/{idStage}")
    public ResponseEntity<Stage> get(@PathVariable Long idStage) {
        final long start = System.currentTimeMillis();
        final String _methodName = "get(idStage)";
        LoggingUtility.logEntering(logger, _methodName, idStage.toString());

        Stage result = stageService.get(idStage);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @GetMapping("stage/season/{idSeason}")
    public ResponseEntity<List<Stage>> listBySeason(@PathVariable Long idSeason) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listBySeason(idSeason)";
        LoggingUtility.logEntering(logger, _methodName, idSeason.toString());

        List<Stage> result = stageService.listBySeason(idSeason);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("stage/")
    @Override
    public ResponseEntity<Stage> save(@RequestBody Stage stage) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(stage)";
        LoggingUtility.logEntering(logger, _methodName, stage.toString());

        if (stage.getIdStage() == null) {
            if (stage.getPublish() == null) {
                stage.setPublish(false);
            }

            if (stage.getShowResults() == null) {
                stage.setShowResults(false);
            }
        }

        Stage result = stageService.save(stage);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("stage/delete/{idStage}")
    @Override
    public ResponseEntity delete(@PathVariable Long idStage) {
        final long start = System.currentTimeMillis();
        final String _methodName = "delete(id)";
        LoggingUtility.logEntering(logger, _methodName, idStage.toString());

        Stage stage = stageService.get(idStage);
        stageService.delete(stage);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok("");
    }
}
