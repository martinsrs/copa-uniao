package com.synit.copauniao.controller;

import com.synit.copauniao.entity.Post;
import com.synit.copauniao.entity.Post;
import com.synit.copauniao.service.BlogService;
import com.synit.copauniao.util.log.LoggingUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
public class BlogController extends BaseController implements Controller<Post> {

    private static Logger logger = Logger.getLogger(BlogController.class);

    @Autowired
    private BlogService blogService;

    @GetMapping("blog/")
    @Override
    public ResponseEntity<List<Post>> list() {
        final long start = System.currentTimeMillis();
        final String _methodName = "list()";
        LoggingUtility.logEntering(logger, _methodName);

        List<Post> result = blogService.list();

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @GetMapping("blog/list")
    public ResponseEntity<Page<Post>> listAvailable(Pageable pageable) {
        final long start = System.currentTimeMillis();
        final String _methodName = "listAvailable()";
        LoggingUtility.logEntering(logger, _methodName);

        Page<Post> result = blogService.listAvailable(pageable);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @GetMapping("blog/post/{idPost}")
    public ResponseEntity<Post> get(@PathVariable Long idPost) {
        final long start = System.currentTimeMillis();
        final String _methodName = "get(idPost)";
        LoggingUtility.logEntering(logger, _methodName, idPost.toString());

        Post result = blogService.get(idPost);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("blog/")
    @Override
    public ResponseEntity<Post> save(@RequestBody Post post) {
        final long start = System.currentTimeMillis();
        final String _methodName = "save(post)";
        LoggingUtility.logEntering(logger, _methodName, post.toString());

        if (post.getPostDate() == null) {
            post.setPostDate(new Date());
        }
        if (post.getIdPost() == null) {
            post.setPublish(false);
        }


        Post result = blogService.save(post);

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok(result);
    }

    @PostMapping("blog/delete/{idPost}")
    @Override
    public ResponseEntity delete(@PathVariable Long idPost) {
        final long start = System.currentTimeMillis();
        final String _methodName = "delete(idPost)";
        LoggingUtility.logEntering(logger, _methodName, idPost.toString());

        Post team = blogService.get(idPost);
        if (team != null) {
            blogService.delete(team);
        }

        LoggingUtility.logTimedMessage(logger, _methodName, start);
        return ResponseEntity.ok("");
    }
}
