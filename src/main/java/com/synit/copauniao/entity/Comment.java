package com.synit.copauniao.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long idComment;
    private String comment;
    private Date commentDate;

    @ManyToOne
    private AdminUser author;

    @ManyToOne
    private Post post;

    private Boolean approved;


    public Long getIdComment() {
        return idComment;
    }

    public void setIdComment(Long idComment) {
        this.idComment = idComment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public AdminUser getAuthor() {
        return author;
    }

    public void setAuthor(AdminUser author) {
        this.author = author;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "idComment=" + idComment +
                ", comment='" + comment + '\'' +
                ", author=" + author +
                ", post=" + post +
                ", approved=" + approved +
                '}';
    }
}
