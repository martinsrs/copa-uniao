package com.synit.copauniao.entity;

import javax.persistence.*;

@Entity
public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long idSubscription;

    @ManyToOne
    private Athlete athlete;

    @ManyToOne
    private Team team;

    @ManyToOne
    private Season season;

    @ManyToOne
    private Stage stage;

    @ManyToOne
    private Category category;

    private Long number;
    private Long position;
    
    public Long getIdSubscription() {
        return idSubscription;
    }

    public void setIdSubscription(Long idSubscription) {
        this.idSubscription = idSubscription;
    }

    public Athlete getAthlete() {
        return athlete;
    }

    public void setAthlete(Athlete athlete) {
        this.athlete = athlete;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "idSubscription=" + idSubscription +
                ", athlete=" + athlete +
                ", team=" + team +
                ", season=" + season +
                ", stage=" + stage +
                ", category=" + category +
                ", number=" + number +
                ", position=" + position +
                '}';
    }
}
