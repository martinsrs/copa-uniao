package com.synit.copauniao.entity;

import javax.persistence.*;

@Entity
public class StartOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer idStart;
    private String startTime;
    private String duration;
    private Integer startSequence;

    @ManyToOne
    @JoinColumn(name="idStage")
    private Stage stage;

    @ManyToOne
    @JoinColumn(name="idCategory")
    private Category category;


    public Integer getIdStart() {
        return idStart;
    }

    public void setIdStart(Integer idStart) {
        this.idStart = idStart;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getStartSequence() {
        return startSequence;
    }

    public void setStartSequence(Integer startSequence) {
        this.startSequence = startSequence;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
