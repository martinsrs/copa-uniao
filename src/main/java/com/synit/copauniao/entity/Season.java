package com.synit.copauniao.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Season implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long idSeason;
    private String year;
    private String edition;
    private Boolean active;

    @ManyToMany
    private List<Category> categories;

    @OneToMany
    private List<Stage> stages;

    public Long getIdSeason() {
        return idSeason;
    }

    public void setIdSeason(Long idSeason) {
        this.idSeason = idSeason;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<Stage> getStages() {
        return stages;
    }

    public void setStages(List<Stage> stages) {
        this.stages = stages;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "Season{" +
                "idSeason=" + idSeason +
                ", year='" + year + '\'' +
                ", edition='" + edition + '\'' +
                ", active=" + active +
                ", stages=" + stages +
                '}';
    }
}
