package com.synit.copauniao.entity;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="athlete")
public class Athlete implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long idAthlete;
    private String name;
    private String city;
    private String state;

    @OneToOne
    private Team team;

    public Long getIdAthlete() {
        return idAthlete;
    }

    public void setIdAthlete(Long idAthlete) {
        this.idAthlete = idAthlete;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public String toString() {
        return "Athlete{" +
                "idAthlete=" + idAthlete +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", team=" + team +
                '}';
    }
}