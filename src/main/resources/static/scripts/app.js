'use strict';

var _main = angular.module('cucSite', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'textAngular'
  ]).config(MainApp)
    .run(AppRun);


window.routes =
    {
        '/' : {
            templateUrl : 'scripts/main/MainView.html',
            controller  : 'MainCtrl',
            controllerAs: 'main'
        },
        '/blog' : {
            templateUrl : 'scripts/blog/BlogView.html',
            controller  : 'BlogViewController',
            controllerAs: 'blog'
        },
        '/blog/post/:idPost' : {
            templateUrl : 'scripts/blog/PostView.html',
            controller  : 'BlogViewController',
            controllerAs: 'blog'
        },
        '/calendar' : {
            templateUrl : 'scripts/calendar/CalendarView.html',
            controller  : 'CalendarController',
            controllerAs: 'calendar'
        },
        '/season/:idSeason/results' : {
            templateUrl : 'scripts/season/ResultsView.html',
            controller  : 'SeasonResultsController',
            controllerAs: 'results'
        },
        '/calendar/:idStage' : {
            templateUrl : 'scripts/calendar/StageView.html',
            controller  : 'CalendarController',
            controllerAs: 'calendar'
        },
        '/login' : {
            templateUrl : 'scripts/login/LoginView.html',
            controller  : 'LoginController',
            controllerAs: 'loginCtrl'
        },
        '/logout' : {
            templateUrl : 'scripts/login/LoginView.html',
            controller  : 'LoginController',
            controllerAs: 'loginCtrl',
            requireLogin: true
        },
        '/admin/blog' : {
            templateUrl : 'scripts/admin/blog/BlogEdit.html',
            controller  : 'BlogController',
            controllerAs: 'blog',
            requireLogin: true
        },
        '/admin/blog/post/:idPost' : {
            templateUrl : 'scripts/admin/blog/PostEdit.html',
            controller  : 'BlogController',
            controllerAs: 'blog',
            requireLogin: true
        },
        '/admin/blog/post/' : {
            templateUrl : 'scripts/admin/blog/PostEdit.html',
            controller  : 'BlogController',
            controllerAs: 'blog',
            requireLogin: true
        },
        '/admin/season' : {
            templateUrl : 'scripts/admin/season/SeasonEdit.html',
            controller  : 'SeasonController',
            controllerAs: 'seasonCtrl',
            requireLogin: true
        },
        '/admin/category' : {
            templateUrl : 'scripts/admin/category/CategoryEdit.html',
            controller  : 'CategoryController',
            controllerAs: 'categoryCtrl',
            requireLogin: true
        },
        '/admin/stage' : {
            templateUrl : 'scripts/admin/stage/StageList.html',
            controller  : 'StageController',
            controllerAs: 'stageCtrl',
            requireLogin: true
        },
        '/admin/stage/new/:idSeason' : {
            templateUrl : 'scripts/admin/stage/StageEdit.html',
            controller  : 'StageController',
            controllerAs: 'stageCtrl',
            requireLogin: true
        },
        '/admin/stage/edit/:idStage' : {
            templateUrl : 'scripts/admin/stage/StageEdit.html',
            controller  : 'StageController',
            controllerAs: 'stageCtrl',
            requireLogin: true
        },
        '/admin/athlete' : {
            templateUrl : 'scripts/admin/athlete/AthleteEdit.html',
            controller  : 'AthleteController',
            controllerAs: 'athleteCtrl',
            requireLogin: true
        },
        '/admin/team' : {
            templateUrl : 'scripts/admin/team/TeamEdit.html',
            controller  : 'TeamController',
            controllerAs: 'teamCtrl',
            requireLogin: true
        },
        '/admin/subscription' : {
            templateUrl : 'scripts/admin/subscription/SubscriptionEdit.html',
            controller  : 'SubscriptionController',
            controllerAs: 'subsCtrl',
            requireLogin: true
        },
        '/admin/stage/subscription/:idSeason/:idStage' : {
            templateUrl : 'scripts/admin/stage/SubscriptionView.html',
            controller  : 'StageSubscriptionController',
            controllerAs: 'stageSubsCtrl',
            requireLogin: true
        },
        '/admin/stage/results/:idSeason/:idStage' : {
            templateUrl : 'scripts/admin/stage/ResultsView.html',
            controller  : 'StageResultsController',
            controllerAs: 'stageResultsCtrl',
            requireLogin: true
        },
        '/admin/profile' : {
            templateUrl : 'scripts/admin/profile/ProfileView.html',
            controller  : 'ProfileController',
            controllerAs: 'profile',
            requireLogin: true
        },
        '/admin/users' : {
            templateUrl : 'scripts/admin/users/UsersView.html',
            controller  : 'UsersController',
            controllerAs: 'usersCtrl',
            requireLogin: true
        },
    };


MainApp.$inject = ['$routeProvider', '$provide'];
function MainApp($routeProvider, $provide) {

    $provide.decorator('taOptions', ['taRegisterTool', '$delegate', taOptions]);

    for(var path in window.routes) {
        $routeProvider.when(path, window.routes[path]);
    }
    $routeProvider.otherwise({redirectTo: '/'});

}

AppRun.$inject = [ '$rootScope', '$location', 'LoginService' ];
function AppRun($rootScope, $location, LoginService){

    $rootScope.$on("$locationChangeStart", function(event, next, current) {
        for(var i in window.routes) {
            if(next.indexOf(i) != -1) {
                if(window.routes[i].requireLogin && !LoginService.getUserAuthenticated()) {
                    event.preventDefault();
                    $location.path('/login');
                }
            }
        }
    });

}

function taOptions(taRegisterTool, taOptions) {
    taOptions.toolbar = [
        ['h1', 'h2', 'h3', 'h6', 'p', 'quote'],
        ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'clear'],
        ['justifyLeft', 'justifyCenter', 'justifyFull'],
        ['html', 'insertImage','insertLink']
    ];

    return taOptions;
}