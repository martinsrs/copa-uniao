'use strict';

_main.directive('switch', Switch);

function Switch(){
    return {
        template : '<input type="checkbox" class="switch-field" data-toggle="toggle" checked="{{switch.active}}">',
        restrict: 'EA',
        scope : {
            ngModel : '=',
            id      : '@'
        },
        controller : SwitchController,
        controllerAs : 'switch',
        link: function(scope, element, attrs){
            $(element).bootstrapToggle({
                on: 'Sim',
                off: 'Não'
            });
        }
    };
}

SwitchController.$inject = ['$scope'];
function SwitchController($scope) {

    this.active = $scope.ngModel;
    var _t = this;

    $scope.$watch('ngModel', function (newVal) {

        console.log(newVal);
        console.log($scope.id);

        _t.active = newVal;
        $('.switch-field').prop('checked', newVal).change();
    });
}

