'use strict';

_main.directive('selectpicker', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).selectpicker();
            
            // When the state is change to, or reloaded...
            scope.$watch(function () { return element[0].options.length; }, 
                function (newValue, oldValue) {
                    if (newValue !== oldValue) {
                        $(element).selectpicker('refresh');
                    }
                }
            );
        }
    };
});