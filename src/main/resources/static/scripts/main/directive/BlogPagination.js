'use strict';

_main.directive('blogPagination', BlogPagination);
function BlogPagination() {

    return {
        template: '<nav aria-label="navigation" ng-show="pagination.paginate">' +
            '  <ul class="pagination">' +
            '    <li class="page-item" ng-class="pagination.disableClass(pagination.disabledFirst)"><a href="" class="page-link" ng-click="pagination.first()">Primeira</a></li>' +
            '    <li class="page-item" ng-class="pagination.disableClass(pagination.disabledFirst)"><a href="" class="page-link" ng-click="pagination.back()">Voltar</a></li>' +
            '    <li class="page-item" ng-class="pagination.activePage(page)" ng-repeat="page in pagination.pages"><a href="" class="page-link" ng-click="pagination.goto(page)">{{page}}</a></li>' +
            '    <li class="page-item" ng-class="pagination.disableClass(pagination.disabledLast)"><a href="" class="page-link" ng-click="pagination.forward()">Próxima</a></li>' +
            '    <li class="page-item" ng-class="pagination.disableClass(pagination.disabledLast)"><a href="" class="page-link" ng-click="pagination.last()">Última</a></li>' +
            '  </ul>' +
            '</nav>',
        restrict: 'E',
        scope : {
            list : '='
        },
        controller: BlogPaginationCtrl,
        controllerAs : 'pagination',
        link: function (scope, elem, attr) {

        }
    };
}

BlogPaginationCtrl.$inject = ['$scope', '$rootScope', 'AppConfig'];
function BlogPaginationCtrl($scope, $rootScope, AppConfig) {

    var _t = this;
    this.currentPage = 0;

    this.activePage = function (page) {
        var result = '';
        var _p = page - 1;
        if (_p == _t.currentPage) {
            result = 'active'
        }

        return result;
    }

    this.disableClass = function (flag) {
        return (flag == true) ? 'disabled' : '';
    }

    this.loadPages = function(max) {
        var pages = [];
        for (var i = 0; i < max; i ++) {
            pages.push(i+1);
        }
        _t.pages = pages;
    }

    this.first = function() {
        if (!_t.disabledFirst) {
            _t.goto(0);
        }
    }

    this.last = function() {
        if (!_t.disabledLast) {
            _t.goto($scope.list.totalPages);
        }
    }

    this.goto = function(page) {

        _t.disabledFirst = false;
        _t.disabledLast  = false;

        if (page <= 1) {
            _t.disabledFirst = true;
            page = 1;
        }
        if (page >= $scope.list.totalPages) {
            _t.disabledLast = true;
            page = $scope.list.totalPages;
        }
        _t.currentPage = page - 1;
        $(document).scrollTop(0);
        $scope.$emit('blog-page', _t.currentPage);

    }

    this.forward = function() {
        if (!_t.disabledLast) {
            var toPage = _t.currentPage + 2;
            _t.goto(toPage);
        }
    }

    this.back = function() {
        if (!_t.disabledFirst) {
            var toPage = _t.currentPage;
            _t.goto(toPage);
        }
    }

    $scope.$watch('list', function (newVal) {
        if ($scope.list != null) {
            _t.disabledFirst = $scope.list.first;
            _t.disabledLast  = $scope.list.last;

            _t.paginate = $scope.list.totalPages > 1;
            _t.loadPages($scope.list.totalPages);
            _t.currentPage = $scope.list.number;
        }
    });

}