'use strict';

_main.directive('formatDate', FormatDate);
function FormatDate() {

	return {
		template: '{{displayValue}}',
		restrict: 'E',
        scope : {
            format : '@',
            value  : '@'
        },
		controller: FormatDateCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

FormatDateCtrl.$inject = ['$scope', 'AppConfig'];

function FormatDateCtrl($scope, AppConfig) {
    $scope.displayValue = "";

    var format = AppConfig.dateFormat[$scope.format];
    if (format == null) {
        format = AppConfig.dateFormat.default;
    }
    moment.locale(AppConfig.dateLocale);    
    var formattedDate = moment($scope.value).format(format);    
    $scope.displayValue = formattedDate;
    
}