'use strict';

_main.directive('switchButton', SwitchButton);

function SwitchButton(){
    return {
        template : '<button type="submit" class="btn btn-success btn-sm" ng-show="ngModel" ' +
                        'data-toggle="tooltip" tooltip data-placement="top" title="{{messageTrue}}">' +
                        '<i class="fa fa-check" aria-hidden="true"></i>' +
                    '</button>' +
                    '<button type="submit" class="btn btn-warning btn-sm" ng-hide="ngModel" ' +
                        'data-toggle="tooltip" tooltip data-placement="top" title="{{messageFalse}}">' +
                        '<i class="fa fa-times" aria-hidden="true"></i>' +
                    '</button>',
        restrict: 'EA',
        scope : {
            ngModel : '=',
            messageTrue   : '@',
            messageFalse  : '@'
        },
        controller : SwitchButtonController,
        controllerAs : 'switch',
        link: function(scope, element, attrs) {
        }
    };
}

SwitchButtonController.$inject = ['$scope'];
function SwitchButtonController($scope) {

}

