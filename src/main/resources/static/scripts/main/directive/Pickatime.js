'use strict';

angular.module('cucSite').directive('pickatime', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).pickatime({
                clear : 'Limpar',
                min: [8,30],
                max: [15,0],
                format : 'HH!him',
                highlighted : 1,
                interval: 15
            });
        }
    };
});