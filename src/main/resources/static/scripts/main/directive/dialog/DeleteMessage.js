'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:
 * @description
 * # DeleteMessage
 */
angular.module('cucApp').directive('deleteMessage', DeleteMessage);

function DeleteMessage() {

	return {
		template: '<div ng-transclude></div>',
		restrict: 'E',
        scope : {
            message : '='
        },
        transclude: true,
        require : '^deleteDialog',
		controller: DeleteMessageCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

DeleteMessageCtrl.$inject = ['$scope', 'AppConfig'];

function DeleteMessageCtrl($scope, AppConfig) {
    
}