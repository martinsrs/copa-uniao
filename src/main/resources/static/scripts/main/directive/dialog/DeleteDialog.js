'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:DeleteDialog
 * @description
 * # DeleteDialog
 */
angular.module('cucApp').directive('deleteDialog', DeleteDialog);

function DeleteDialog() {

	return {
		templateUrl: 'scripts/main/directive/dialog/DeleteDialog.html',
		restrict: 'E',
        scope : {
            deleteAction : '&'
        },
        transclude: {
            
        },
		controller: DeleteDialogCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

DeleteDialogCtrl.$inject = ['$scope', 'AppConfig'];

function DeleteDialogCtrl($scope, AppConfig) {
    
}