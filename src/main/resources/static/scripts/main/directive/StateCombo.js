'use strict';


_main.directive('stateCombo', StateCombo);
function StateCombo() {

	return {
		template: '<select class="form-control" ng-model="ngModel">' +
                      '<option value="" disabled selected>Selecione um Estado...</option>' + 
                      '<option ng-repeat="state in combo.states" value="{{state.initials}}">{{state.name}}</option>' +
                  '</select>',
		restrict: 'E',
        scope : {
            ngModel : '='
        },
		controller: StateComboCtrl,
        controllerAs : 'combo',
		link: function (scope, elem, attr) {
            
		}
    };
}

StateComboCtrl.$inject = ['$scope'];
function StateComboCtrl($scope) {
    this.states = [];
    this.states.push({ name: 'Acre', initials: 'AC' });
    this.states.push({ name: 'Alagoas', initials: 'AL' });
    this.states.push({ name: 'Amapá', initials: 'AP' });
    this.states.push({ name: 'Amazonas', initials: 'AM' });
    this.states.push({ name: 'Bahia', initials: 'BA' });
    this.states.push({ name: 'Ceará', initials: 'CE' });
    this.states.push({ name: 'Distrito Federal', initials: 'DF' });
    this.states.push({ name: 'Espírito Santo', initials: 'ES' });
    this.states.push({ name: 'Goiás', initials: 'GO' });
    this.states.push({ name: 'Maranhão', initials: 'MA' });
    this.states.push({ name: 'Mato Grosso', initials: 'MT' });
    this.states.push({ name: 'Mato Grosso do Sul', initials: 'MS' });
    this.states.push({ name: 'Minas Gerais', initials: 'MG' });
    this.states.push({ name: 'Pará', initials: 'PA' });
    this.states.push({ name: 'Paraíba', initials: 'PB' });
    this.states.push({ name: 'Paraná', initials: 'PR' });
    this.states.push({ name: 'Pernambuco', initials: 'PE' });
    this.states.push({ name: 'Piauí', initials: 'PI' });
    this.states.push({ name: 'Rio de Janeiro', initials: 'RJ' });
    this.states.push({ name: 'Rio Grande do Norte', initials: 'RN' });
    this.states.push({ name: 'Rio Grande do Sul', initials: 'RS' });
    this.states.push({ name: 'Rondônia', initials: 'RO' });
    this.states.push({ name: 'Roraima', initials: 'RR' });
    this.states.push({ name: 'Santa Catarina', initials: 'SC' });
    this.states.push({ name: 'São Paulo', initials: 'SP' });
    this.states.push({ name: 'Sergipe', initials: 'SE' });
    this.states.push({ name: 'Tocantins', initials: 'TO' });
}