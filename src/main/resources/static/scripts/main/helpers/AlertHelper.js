'use strict';

_main.service('AlertHelper', AlertHelper);

AlertHelper.$inject = [];
function AlertHelper() {

    this.success = function(message) {
        notify('success', message);
    }

    this.info = function(message) {
        notify('info', message);
    }

    this.warning = function(message) {
        notify('warning', message);
    }

    this.error = function(message) {
        notify('error', message);
    }
}

function notify(type, message) {

    var icon = '';
    if (type == 'success') {
        icon = 'fa fa-check';
    }
    if (type == 'error') {
        icon = 'fa fa-times';
        type = 'danger';
    }
    if (type == 'warning') {
        icon = 'fa fa-exclamation-triangle';
    }
    if (type == 'info') {
        icon = 'fa fa-info-circle';
    }

    var settings = {
        element: 'body',
        position: null,
        type: type,
        allow_dismiss: true,
        newest_on_top: true,
        placement: {
            from: "bottom",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 2000,
        timer: 1000,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        icon_type: 'class'
    };

    $.notify({
        icon: icon,
        title: '',
        message: message
    },settings);

}