'use strict';

_main.controller('CalendarController', CalendarController);
CalendarController.$inject = ['$scope', '$q', '$routeParams', 'AppConfig', 'StageService', 'SubscriptionService'];
function CalendarController($scope, $q, $routeParams, AppConfig, StageService, SubscriptionService) {

    var _t = this;

    this.load = function () {
        $q.all({
            stages : StageService.list()
        }).then(function(result) {
            _t.list = result.stages.data;
        });
    }

    this.loadStage = function (id) {
        _t.subscriptions = [];

        $q.all({
            stages : StageService.get(id)
        }).then(function(result) {
            _t.stage = result.stages.data;
            _t.stage.dateFormated = moment(_t.stage.date, AppConfig.dateFormat.default).format(AppConfig.dateFormat.short);

            var byStageList = {};
            _t.stage.season.categories.forEach(function (category) {
                $q.all({
                    listBySeasonStageCategory: SubscriptionService.listBySeasonStageCategory(_t.stage.season.idSeason, _t.stage.idStage, category.idCategory)
                }).then(function (result) {
                    var list = result.listBySeasonStageCategory.data;
                    if (list != null && list.length > 0) {
                        byStageList = list[0].category;
                        byStageList.list = list;

                        _t.subscriptions.push(byStageList);
                        _t.subscriptions.sort(SortCategoryName);
                    }
                });

            });
        });
    }

    this.load();

    if ($routeParams != null) {
        if ($routeParams.idStage != null) {
            var stage = $routeParams.idStage;
            _t.loadStage(stage);
        }
    }
}
