'use strict';

_main.controller('BlogViewController', BlogViewController);
BlogViewController.$inject = ['$scope', '$rootScope', '$routeParams', '$q', 'AppConfig', 'BlogService', 'LoginService'];
function BlogViewController($scope, $rootScope, $routeParams, $q, AppConfig, BlogService, LoginService) {

    var _t = this;

    this.load = function (page) {
        $q.all({
            listPaginated : BlogService.listPaginated(page)
        }).then(function(result) {
            _t.list = result.listPaginated.data;
            console.log(_t.list);
        });
    }

    this.getOne = function (id) {
        $q.all({
            getOne : BlogService.getOne(id)
        }).then(function(result) {
            _t.post = result.getOne.data;
            _t.post.postDateFormatted = moment(_t.post.postDate, AppConfig.dateFormat.default).format(AppConfig.dateFormat.short);
            console.log(_t.post);
        });
    }

    this.save = function (post) {
        var user = LoginService.getUserSession();
        post.author = user;

        $q.all({
            save : BlogService.save(post)
        }).then(function(result) {
            _t.post = result.save.data;
            $rootScope.$broadcast('post-saved', {});
        });
    }

    $scope.$on('blog-page', function(event, data) {
        console.log(data);
        _t.load(data);
    });

    if ($routeParams != null) {
        if ($routeParams.idPost != null) {
            var idPost = $routeParams.idPost;
            _t.getOne(idPost);
        } else {
            var page = ($routeParams.page != null) ? $routeParams.page : 0;
            _t.load(page);
        }
    } else {
        _t.load(0);
    }

}
