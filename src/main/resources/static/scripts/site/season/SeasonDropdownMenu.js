'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:SeasonDropdownMenu
 * @description
 * # SeasonDropdownMenu
 */
_main.directive('seasonDropdownMenu', SeasonDropdownMenu);

function SeasonDropdownMenu() {

    return {

        template : '<li ng-repeat="season in seasonDropdown.list"><a ng-href="#/season/{{season.idSeason}}/results"><i class="fa fa-calendar fa-fw"></i> {{season.year}} - {{season.edition}}</a>\n' +
            '</li>',

        restrict: 'AE',
        scope : {
            ngModel : '='
        },
        controller: SeasonDropdownMenuCtrl,
        controllerAs : 'seasonDropdown',
        link: function (scope, elem, attr) {

        }
    };
}

SeasonDropdownMenuCtrl.$inject = ['$scope', '$q', 'SeasonService'];

function SeasonDropdownMenuCtrl($scope, $q, SeasonService) {
    this.list = [];

    var _t = this;

    this.reload = function() {
        $q.all({
            seasons: SeasonService.listActive()
        }).then(function(result) {
            _t.list = result.seasons.data;
        });
    }

    $scope.$on('season-saved', function (event) {
        _t.reload();
    });

    this.reload();
}