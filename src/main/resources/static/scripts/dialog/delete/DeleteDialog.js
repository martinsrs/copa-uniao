'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:DeleteDialog
 * @description
 * # DeleteDialog
 */
_main.directive('deleteDialog', DeleteDialog);

function DeleteDialog() {

	return {
		templateUrl: 'scripts/dialog/delete/DeleteDialog.html',
		restrict: 'E',
        scope : {
            deleteAction : '&'
        },
        transclude: {
            
        },
		controller: DeleteDialogCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

DeleteDialogCtrl.$inject = ['$scope'];

function DeleteDialogCtrl($scope) {
    
}