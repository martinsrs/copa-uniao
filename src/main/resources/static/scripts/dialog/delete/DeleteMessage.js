'use strict';

/**
 * @ngdoc directive
 * @name cucSite.directive:
 * @description
 * # DeleteMessage
 */
_main.directive('deleteMessage', cc);

function DeleteMessage() {

	return {
		template: '<div ng-transclude></div>',
		restrict: 'E',
        scope : {
            message : '='
        },
        transclude: true,
        require : '^deleteDialog',
		controller: DeleteMessageCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

DeleteMessageCtrl.$inject = ['$scope'];
function DeleteMessageCtrl($scope) {
    
}