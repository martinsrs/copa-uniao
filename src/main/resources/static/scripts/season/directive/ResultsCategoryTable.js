'use strict';

_main.directive('resultsCategoryTable', ResultsCategoryTable);
function ResultsCategoryTable() {

    return {
        template: '<div class="col-md-12" ng-repeat="category in list">\n' +
            '    <h4>Categoria {{category.name}}</h4>\n' +
            '    <div class="table-resonsive">\n' +
            '        <table class="table table-striped table-bordered table-hover">\n' +
            '            <thead>\n' +
            '            <tr>\n' +
            '                <th class="col-md-1">#</th>\n' +
            '                <th class="col-md-5">Nome</th>\n' +
            '                <th class="col-md-3">Equipe</th>\n' +
            '                <th class="col-md-2">Cidade</th>\n' +
            '                <th class="col-md-1">Pontos</th>\n' +
            '            </tr>\n' +
            '            </thead>\n' +
            '            <tbody>\n' +
            '            <tr ng-repeat="item in category.list">\n' +
            '                <td>{{$index + 1}}</td>\n' +
            '                <td>{{item.athlete.name}}</td>\n' +
            '                <td>{{item.athlete.team.name}}</td>\n' +
            '                <td>{{item.athlete.city}} / {{item.athlete.state}}</td>\n' +
            '                <td>{{item.score}}</td>\n' +
            '            </tr>\n' +
            '            </tbody>\n' +
            '        </table>\n' +
            '    </div>\n' +
            '</div>',
        restrict: 'E',
        scope : {
            list : '='
        },
        controller: ResultsCategoryTableCtrl,
        link: function (scope, elem, attr) {

        }
    };
}

ResultsCategoryTableCtrl.$inject = ['$scope', 'AppConfig'];
function ResultsCategoryTableCtrl($scope, AppConfig) {

}