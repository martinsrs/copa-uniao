'use strict';

_main.controller('SeasonResultsController', SeasonResultsController);
SeasonResultsController.$inject = ['$scope', '$routeParams', '$q', 'SeasonService', 'ResultsService'];
function SeasonResultsController($scope, $routeParams, $q, SeasonService, ResultsService) {

    var _t = this;

    this.load = function (idSeason) {
        $q.all({
            season : SeasonService.get(idSeason),
            team   : ResultsService.listTeamBySeason(idSeason)
        }).then(function(result) {
            _t.season = result.season.data;
            _t.teamResults = result.team.data;

            _t.allCategories = [];
            var categoryResult = {}
            _t.season.categories.forEach(function (category) {

                $q.all({
                    category : ResultsService.listBySeasonCategory(idSeason, category.idCategory),
                }).then(function(result) {
                    var list = result.category.data;

                    if (list != null && list.length > 0) {
                        categoryResult = list[0].category;
                        categoryResult.list = list;

                        _t.allCategories.push(categoryResult);

                        _t.allCategories.sort(SortCategoryName);
                    }
                });

            });
        });
    }

    this.loadStage = function (idSeason, idStage) {
        _t.byStage = [];
        var byStageResult = {}
        _t.season.categories.forEach(function (category) {
            $q.all({
                listBySeasonStage: ResultsService.listBySeasonStage(idSeason, idStage, category.idCategory)
            }).then(function (result) {
                var list = result.listBySeasonStage.data;
                if (list != null && list.length > 0) {
                    byStageResult = list[0].category;
                    byStageResult.list = list;

                    _t.byStage.push(byStageResult);
                    _t.byStage.sort(SortCategoryName);
                }
            });
        });
    }

    $scope.$watch('results.selectedStage', function (newVal) {
       if (_t.selectedStage != null) {
           var season = $routeParams.idSeason;
           var stage  = _t.selectedStage;
           _t.loadStage(season, stage);
       }
    });

    if ($routeParams != null) {
        if ($routeParams.idSeason != null) {
            var season = $routeParams.idSeason;
            _t.load(season);
        }
    }

}

function SortCategoryName(a, b){
    if(a.name < b.name) return -1;
    if(a.name > b.name) return 1;
    return 0;
}
