'use strict';

_main.directive('adminNavbar', AdminNavbar);
function AdminNavbar() {

    return {

        templateUrl : 'scripts/admin/AdminNavbar.html',
        restrict: 'AE',
        scope : {
            ngModel : '='
        },
        controller: AdminNavbarCtrl,
        controllerAs: 'adminNavbar',
        link: function (scope, elem, attr) {

        }
    };
}

AdminNavbarCtrl.$inject = ['$scope', 'LoginService'];

function AdminNavbarCtrl($scope, LoginService) {
    var _t = this;
    this.authenticated = LoginService.getUserAuthenticated();

    $scope.$on('logout', function (event) {
        _t.authenticated = LoginService.getUserAuthenticated();
    });

    $scope.$on('login', function (event) {
        _t.authenticated = LoginService.getUserAuthenticated();
    });

}