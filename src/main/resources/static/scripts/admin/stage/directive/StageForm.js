'use strict';

_main.directive('stageForm', StageForm);
function StageForm() {

    return {
        templateUrl: 'scripts/admin/stage/directive/StageForm.html',
        restrict: 'E',
        scope : {
            season : '=',
            stage  : '='
        },
        controller: StageFormCtrl,
        controllerAs : 'form',
        link: function (scope, elem, attr) {
            $('.datepicker').pickadate({format: 'dd/mm/yyyy', formatSubmit : 'yyyy-mm-dd'});
        }
    };
}

StageFormCtrl.$inject = ['$scope', '$rootScope', 'AppConfig', '$q', 'SeasonService', 'StageService'];
function StageFormCtrl($scope, $rootScope, AppConfig, $q, SeasonService, StageService) {

    this.stage = {};
    var _t = this;

    this.loadSeason = function (idSeason) {
        if (idSeason != null) {
            $q.all({
                season : SeasonService.get(idSeason)
            }).then(function(result) {
                _t.stage.season = result.season.data;
            });
        }
    }

    this.save = function (vo) {
        var currentStage = vo;
        currentStage.date = moment(_t.stage.date, AppConfig.dateFormat.short).format(AppConfig.dateFormat.default);
        currentStage.date = currentStage.date + AppConfig.dateTimeFix;
        currentStage.season = _t.stage.season;

        console.log(vo);
        console.log(currentStage);
        $q.all({
            stage : StageService.save(currentStage)
        }).then(function(result) {
            _t.stage = result.stage.data;
            $rootScope.$broadcast('stage-saved', {});
        });

        _t.stage = {};
        _t.loadSeason($scope.season);

        $rootScope.$broadcast('stage-saved', {});
    }

    $scope.$watch('season', function() {
        _t.loadSeason($scope.season);
    });

    $scope.$watch('stage', function() {
        _t.stage = $scope.stage;

        if (_t.stage != null && _t.stage.date != null) {
            var dateFormat = formatDate(_t.stage.date, AppConfig.dateFormat.short);
            _t.stage.date = dateFormat;

            var $input = $('.datepicker').pickadate();
            var picker = $input.pickadate('picker');
            picker.set('select', dateFormat, { format: 'dd/mm/yyyy' });
        } else {
            var $input = $('.datepicker').pickadate();
            var picker = $input.pickadate('picker');
            picker.set('select', new Date(), { format: 'dd/mm/yyyy' });
            picker.clear();
        }

        _t.loadSeason($scope.season);
    });
}



function formatDate(date, outputFormat) {
    var formatted;
    var inputFormat = 'YYYY-MM-DD';

    if (date.indexOf('/') > 0) {
        inputFormat = 'DD/MM/YYYY';
    }
    return moment(date, inputFormat).format(outputFormat);
}