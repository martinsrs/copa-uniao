'use strict';

_main.directive('stageTable', StageTable);
function StageTable() {

    return {
        templateUrl: 'scripts/admin/stage/directive/StageTable.html',
        restrict: 'E',
        scope : {

            season : '='

        },
        controller   : StageTableCtrl,
        controllerAs : 'table',
        link: function (scope, elem, attr) {

        }
    };
}

StageTableCtrl.$inject = ['$scope', '$q', 'StageService', 'AppConfig', 'SubscriptionService'];
function StageTableCtrl($scope, $q, StageService, AppConfig, SubscriptionService) {

    var _t = this;
    this.seasonSelected = false;

    this.reload = function () {
        var idSeason = $scope.season;
        $q.all({
            stages : StageService.listBySeason(idSeason)
        }).then(function(result) {
            _t.list = result.stages.data;
            _t.list.forEach(function (stage) {
                $q.all({
                    subs : SubscriptionService.listBySeasonStage(stage.season.idSeason, stage.idStage)
                }).then(function(result) {
                    var list = result.subs.data;
                    var count = list.length;
                    stage.subscriptions = count;
                });
            });
        });
    }

    this.save = function(vo) {

        vo.date = vo.date + AppConfig.dateTimeFix;

        $q.all({
            stage : StageService.save(vo)
        }).then(function(result) {
            $scope.$broadcast('stage-saved', {});
        });

    }

    this.setPublish = function(vo, flag) {
        vo.publish = flag;
        _t.save(vo);
    }

    this.setShowResults = function(vo, flag) {
        vo.showResults = flag;
        _t.save(vo);
    }

    this.prepareDel = function(vo) {
        $scope.toDeleteVo = vo;
    }

    this.newStage = function() {
        _t.editStage = {};
        _t.season = $scope.season;
    }

    this.turnEdit = function(vo) {
        _t.editStage = vo;
    }

    this.delete = function(vo) {
        $q.all({
            stage : StageService.delete(vo.idStage)
        }).then(function(result) {
            $scope.$broadcast('stage-saved', {});
        });
    }

    $scope.$watch('season', function() {
        if ($scope.season != null) {
            _t.seasonSelected = true;
            _t.season = $scope.season;
            _t.reload();
        }
    });

    $scope.$on('stage-saved', function(event) {
        _t.reload();
    });
}
