'use strict';


_main.directive('stageCombo', StageCombo);
function StageCombo() {
    return {
        template: '<select class="form-control" ng-model="ngModel">' +
            '<option value="" disabled>Selecione uma Etapa...</option>' +
            '<option ng-repeat="stage in combo.list" value="{{stage.idStage}}">{{stage.dateFormated}} - {{stage.location}}</option>' +
            '</select>',
        restrict: 'E',
        scope : {
            season  : '=',
            ngModel : '='
        },
        controller: StageComboCtrl,
        controllerAs : 'combo',
        link: function (scope, elem, attr) {

        }
    };
}

StageComboCtrl.$inject = ['$scope', '$q', 'AppConfig', 'StageService'];
function StageComboCtrl($scope, $q, AppConfig, StageService ) {

    var _t = this;

    this.load = function () {
        var idSeason = $scope.season;
        $q.all({
            stages : StageService.listBySeason(idSeason)
        }).then(function(result) {
            _t.list = result.stages.data;

            _t.list.forEach(function(stage) {
                stage.dateFormated = moment(stage.date, AppConfig.dateFormat.default).format(AppConfig.dateFormat.short);
            });
        });
    }

    $scope.$watch('season', function() {
        if ($scope.season != null) {
            _t.seasonSelected = true;
            _t.season = $scope.season;
            _t.load();
        }
    });

    $scope.$on('stage-saved', function(event) {
        _t.load();
    });
}