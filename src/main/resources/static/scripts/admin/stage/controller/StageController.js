'use strict';

/**
 * @ngdoc function
 * @name cucSite.controller:StageController
 * @description
 * # StageController
 * Controller of the cucSite
 */
_main.controller('StageController', StageController);

StageController.$inject = ['$scope', '$rootScope', '$route', '$routeParams', '$q', 'AppConfig', 'SeasonService', 'StageService'];
function StageController($scope, $rootScope, $route, $routeParams, $q, AppConfig, SeasonService, StageService) {

    var _t = this;
    this.getSeason = function (id) {
        $q.all({
            getOne : SeasonService.get(id)
        }).then(function(result) {
            _t.season = result.getOne.data;
            _t.stage = {};
            _t.stage.season = _t.season;
            console.log(_t.season);
        });
    }

    this.getOne = function (id) {
        $q.all({
            getOne : StageService.get(id)
        }).then(function(result) {
            _t.stage = result.getOne.data;
            _t.fixDate();
        });
    }

    this.save = function (vo) {
        var currentStage = vo;

        currentStage.date = moment(_t.stage.date, AppConfig.dateFormat.short).format(AppConfig.dateFormat.default);
        currentStage.date = currentStage.date + AppConfig.dateTimeFix;
        currentStage.season = _t.stage.season;

        $q.all({
            save : StageService.save(currentStage)
        }).then(function(result) {
            _t.stage = result.save.data;
            _t.fixDate();
            $rootScope.$broadcast('stage-saved', {});
        });
    }

    this.fixDate = function () {
        if (_t.stage != null && _t.stage.date != null) {
            var dateFormat = formatDate(_t.stage.date, AppConfig.dateFormat.short);
            _t.stage.date = dateFormat;

            var $input = $('.datepicker').pickadate();
            var picker = $input.pickadate('picker');
            picker.set('select', dateFormat, { format: 'dd/mm/yyyy' });
        }
    }

    if ($routeParams != null) {

        if ($routeParams.idSeason != null) {
            var id = $routeParams.idSeason;
            _t.getSeason(id);
        }

        if ($routeParams.idStage != null) {
            var idStage = $routeParams.idStage;
            _t.getOne(idStage);
        }
    } else {
        _t.load(0);
    }
}