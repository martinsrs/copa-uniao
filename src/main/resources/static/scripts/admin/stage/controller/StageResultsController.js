'use strict';


_main.controller('StageResultsController', StageResultsController);
StageResultsController.$inject = ['$scope', '$rootScope', '$routeParams', 'AppConfig', '$q', 'ResultsService', 'StageService', 'CategoryService'];
function StageResultsController($scope, $rootScope, $routeParams, AppConfig, $q, ResultsService, StageService, CategoryService) {

    var _t = this;
    this.categorySelected = false;

    this.loadStage = function (season, stage) {
        $q.all({
            stage : StageService.get(stage)
        }).then(function(result) {
            _t.stage = result.stage.data;
            _t.stage.dateFormated = moment(_t.stage.date, AppConfig.dateFormat.default).format(AppConfig.dateFormat.short);
        });
    }

    this.load = function (season, stage, category) {
        $q.all({
            list     : ResultsService.listBySeasonStageCategory(season, stage, category),
            stage    : StageService.get(stage),
            category : CategoryService.get(category)
        }).then(function(result) {
            _t.list = result.list.data;
            _t.stage = result.stage.data;
            _t.stage.dateFormated = moment(_t.stage.date, AppConfig.dateFormat.default).format(AppConfig.dateFormat.short);
            _t.category = result.category.data;
        });
    }

    this.save = function(list) {
        $q.all({
            list     : ResultsService.save(list)
        }).then(function(result) {
            var season =$routeParams.idSeason;
            var stage = $routeParams.idStage;
            var category = _t.selectedCategory;
            _t.load(season, stage, category);
        });
    }

    $scope.$watch('stageResultsCtrl.selectedCategory', function (newVal) {
        _t.categorySelected = false;
        if (newVal != null) {
            var season =$routeParams.idSeason;
            var stage = $routeParams.idStage;
            var category = _t.selectedCategory;
            _t.load(season, stage, category);
            _t.categorySelected = true;
        }

    });

    if ($routeParams != null) {
        if ($routeParams.idSeason != null && $routeParams.idStage != null) {
            var season =$routeParams.idSeason;
            var stage = $routeParams.idStage;

            _t.loadStage(season, stage);
        }
    }
}