'use strict';

_main.factory('StageService', StageService);
StageService.$inject = ['$http', 'AlertHelper'];
function StageService($http, AlertHelper) {

    var factory = {};
    factory.list = function(){
        return $http.get('/services/v1/stage/').
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.listBySeason = function(idSeason){
        return $http.get('/services/v1/stage/season/' + idSeason).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.get = function(idStage){
        return $http.get('/services/v1/stage/' + idStage).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.listActive = function(){
        return $http.get('/services/v1/stage/active').
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.save = function(vo){
        return $http.post('/services/v1/stage/', vo).
        success(function(data, status) {
            AlertHelper.success("Etapa salva com sucesso!");
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.delete = function(idSeason){
        return $http.post('/services/v1/stage/delete/' + idSeason).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    return factory;
}