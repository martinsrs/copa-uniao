'use strict';

_main.controller('UsersController', UsersController);
UsersController.$inject = ['$scope', '$q', 'ProfileService', 'LoginService', 'AlertHelper'];
function UsersController($scope, $q, ProfileService, LoginService, AlertHelper) {

    var _t = this;
    this.save = function (user) {

        if (user != null) {
            if (user.tmpPass != null && user.tmpPass == user.tmpPass2) {
                user.password = user.tmpPass;
                $q.all({
                    profile : ProfileService.save(user)
                }).then(function(result) {
                    _t.user = {};
                    $scope.$broadcast('users-saved', {});
                });
            } else {
                AlertHelper.error('As senhas informadas não conferem!');
            }
        }
    }
}
