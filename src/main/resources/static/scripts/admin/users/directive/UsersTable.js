'use strict';

_main.directive('usersTable', UsersTable);
function UsersTable() {

    return {
        templateUrl: 'scripts/admin/users/directive/UsersTable.html',
        restrict: 'E',
        scope : {

        },
        controller    : UsersTableCtrl,
        controllerAs  : 'table',
        link: function (scope, elem, attr) {

        }
    };
}

UsersTableCtrl.$inject = ['$scope', '$rootScope', '$q', 'ProfileService'];
function UsersTableCtrl($scope, $rootScope, $q, ProfileService) {

    var _t = this;

    this.reload = function () {
        $q.all({
            users : ProfileService.list()
        }).then(function(result) {
            _t.list = result.users.data;
        });
    }

    this.turnEdit = function(vo) {
        if (vo.edit == null || vo.edit == false) {
            vo.edit = true;
        } else {
            vo.edit = false;
        }
        return vo;
    }

    this.save = function(vo) {

        $q.all({
            profile : ProfileService.save(vo)
        }).then(function(result) {
            $rootScope.$broadcast('users-saved', {});
        });

    }

    this.popUp = function(vo) {
        _t.popUpVo = vo;
    }

    this.prepareDel = function(vo) {
        _t.toDeleteVo = vo;
    }

    this.delete = function(vo) {
        $q.all({
            categories : ProfileService.delete(vo.idUser)
        }).then(function(result) {
            $rootScope.$broadcast('users-saved', {});
        });
    }

    this.setAdmin = function(user, admin) {
        user.adminRole = admin;
        _t.save(user);
    }

    this.setActive = function(user, active) {
        user.active = active;
        _t.save(user);
    }

    $scope.$on('users-saved', function (event) {
        _t.reload();
    });

    this.reload();
}