'use strict';


_main.controller('TeamController', TeamController);
TeamController.$inject = ['$scope', '$q', 'TeamService'];
function TeamController($scope, $q, TeamService) {


    var _t = this;
    this.load = function () {

        $q.all({
            teams : TeamService.list()
        }).then(function(result) {
            _t.allSeasons = result.teams.data;
        });

    }

    this.load();
}