'use strict';

_main.factory('TeamService', TeamService);
TeamService.$inject = ['$http', 'AlertHelper'];
function TeamService($http, AlertHelper) {

    var factory = {};
    factory.list = function(){
        return $http.get('/services/v1/team/').
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.save = function(vo){
        return $http.post('/services/v1/team/', vo).
        success(function(data, status) {
            AlertHelper.success("Salvo com sucesso!");
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.delete = function(id){
        return $http.post('/services/v1/team/delete/' + id).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    return factory;
}