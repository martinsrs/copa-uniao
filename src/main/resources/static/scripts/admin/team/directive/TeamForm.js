'use strict';

_main.directive('teamForm', TeamForm);
function TeamForm() {

    return {
        template :  '<div class="col-md-12">' +
                        '<h6>Nome da Equipe</h6>' +
                            '<input ng-model="form.teamVo.name" type="text" class="form-control" name="nomeEquipe" required>' +
                        '</div>' +
                        '<div class="col-md-6">' +
                            '<h6>Cidade</h6>' +
                            '<input ng-model="form.teamVo.city" type="text" class="form-control" name="cidadeEquipe" required>' +
                        '</div>' +
                            '<div class="col-md-6">' +
                            '<h6>Estado</h6>' +
                            '<state-combo ng-model="form.teamVo.state"></state-combo>' +
                        '</div>' +
                        '<div class="col-md-12">' +
                            '<h6>&nbsp;</h6>' +
                            '<div class="pull-right" ng-show="modal">' +
                                '<div class="btn-group">' +
                                    '<button type="button" class="btn btn-link" data-dismiss="modal">Cancelar</button>' +
                                '</div>' +
                                '<div class="btn-group">' +
                                    '<button type="submit" class="btn btn-primary" data-dismiss="modal" ng-disabled="form.disableSave()" ng-click="form.save(form.teamVo)">Salvar</button>' +
                                '</div>' +
                            '</div>' +
                            '<div class="btn-group pull-right" ng-hide="modal">' +
                                '<button type="button" class="btn btn-primary" ng-disabled="form.disableSave()" ng-click="form.save(form.teamVo)">Salvar</button>' +
                            '</div>' +
                        '</div>' +
                    '</div>',
        
        restrict: 'E',
        scope : {
            season : '=',
            modal  : '=',
            stage  : '='
        },
        controller: TeamFormCtrl,
        controllerAs : 'form',
        link: function (scope, elem, attr) {

        }
    };
}

TeamFormCtrl.$inject = ['$scope', '$rootScope', '$q', 'TeamService'];
function TeamFormCtrl($scope, $rootScope, $q, TeamService) {

    var _t = this;
    this.teamVo = {}

    this.save = function (vo) {

        $q.all({
            team : TeamService.save(vo)
        }).then(function(result) {

            if ($scope.modal) {
                $('#formEquipe').modal('hide');
            }

            $rootScope.$broadcast('team-saved', {});
        });

        _t.teamVo = {};
    }

    this.disableSave = function() {
        var result = false;
        if (_t.teamVo.name == null || _t.teamVo.name == '') {
            result = true;
        }
        if (_t.teamVo.city == null || _t.teamVo.city == '') {
            result = true;
        }
        if (_t.teamVo.state == null || _t.teamVo.state == '') {
            result = true;
        }

        return result;
    }

}