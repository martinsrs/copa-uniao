'use strict';

_main.directive('teamTable', TeamTable);

function TeamTable() {

    return {
        templateUrl: 'scripts/admin/team/directive/TeamTable.html',
        restrict: 'E',
        scope : {

        },
        controller : TeamTableCtrl,
        controllerAs : 'table',
        link: function (scope, elem, attr) {

        }
    };
}

TeamTableCtrl.$inject = ['$scope', '$rootScope', '$q', 'TeamService'];

function TeamTableCtrl($scope, $rootScope, $q, TeamService) {
    var _t = this;

    this.load = function () {
        $q.all({
            team : TeamService.list()
        }).then(function(result) {
            _t.list = result.team.data;
        });
    }

    this.turnEdit = function(vo) {
        if (vo.edit == null || vo.edit == false) {
            vo.edit = true;
        } else {
            vo.edit = false;
        }
        return vo;
    }

    this.prepareDel = function(vo) {
        _t.toDeleteVo = vo;
    }

    this.save = function(vo) {
        $q.all({
            team : TeamService.save(vo)
        }).then(function(result) {
            $rootScope.$broadcast('team-saved', {});
        });
    }

    this.delete = function(vo) {
        $q.all({
            team : TeamService.delete(vo.idTeam)
        }).then(function(result) {
            $rootScope.$broadcast('team-saved', {});
        });
    }

    this.load();

    $scope.$on('team-saved', function(event) {
        _t.load();
    });
}