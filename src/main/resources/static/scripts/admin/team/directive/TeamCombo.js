'use strict';


_main.directive('teamCombo', TeamCombo);
function TeamCombo() {
    return {
        template: '<select class="form-control" ng-model="ngModel">' +
                        '<option value="" disabled>Selecione uma Equipe...</option>' +
                        '<option ng-repeat="team in combo.list" value="{{team.idTeam}}">{{team.name}}</option>' +
                    '</select>',
        restrict: 'E',
        scope : {
            ngModel : '='
        },
        controller: TeamComboCtrl,
        controllerAs : 'combo',
        link: function (scope, elem, attr) {

        }
    };
}

TeamComboCtrl.$inject = ['$scope', '$q', 'TeamService'];
function TeamComboCtrl($scope, $q, TeamService ) {

    var _t = this;
    this.load = function () {

        $q.all({
            team : TeamService.list()
        }).then(function(result) {
            _t.list = result.team.data;
        });
    }

    this.load();
    $scope.$on('team-saved', function(event) {
        _t.load();
    });
}