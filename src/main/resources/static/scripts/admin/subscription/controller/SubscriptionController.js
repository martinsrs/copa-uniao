'use strict';


_main.controller('SubscriptionController', SubscriptionController);
SubscriptionController.$inject = ['$scope', '$rootScope', '$q', 'SubscriptionService'];
function SubscriptionController($scope, $rootScope, $q, SubscriptionService) {


    var _t = this;
    this.load = function () {

    }

    this.clear = function () {
        _t.subscription.athlete = null;
    }

    this.save = function (vo) {

        vo.team = vo.athlete.team;
        $q.all({
            subscription : SubscriptionService.save(vo)
        }).then(function(result) {
            _t.clear();
            $rootScope.$broadcast('subscription-saved', {});
            $rootScope.$broadcast('stage-changed', {});
        });

    }

    this.disableSave = function () {
        var result = false;

        if (this.subscription != null) {
            if (this.subscription.season == null) {
                result = true;
            }

            if (this.subscription.stage == null) {
                result = true;
            }

            if (this.subscription.athlete == null) {
                result = true;
            }
        } else {
            result = true;
        }

        return result;
    }

    this.cancel = function() {
        _t.clear();
        _t.subscription.stage = null;
        _t.subscription.season = null;
        $rootScope.$broadcast('stage-changed', {});
    }

    $scope.$watch('subsCtrl.subscription.stage.idStage', function (newVal) {
        if (_t.subscription != null && _t.subscription.stage != null && _t.subscription.stage.idStage != null) {
            $rootScope.$broadcast('stage-changed', {});
        }
    });

}