'use strict';

_main.factory('SubscriptionService', SubscriptionService);
SubscriptionService.$inject = ['$http', 'AlertHelper'];
function SubscriptionService($http, AlertHelper) {

    var factory = {};
    factory.list = function(){
        return $http.get('/services/v1/subscription/').
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.listByAthleteSeasonStage = function(idAthlete, idSeason, idStage){
        return $http.get('/services/v1/subscription/athlete/'+ idAthlete +'/season/'+ idSeason +'/stage/' + idStage).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.listBySeasonStage = function(idSeason, idStage){
        return $http.get('/services/v1/subscription/season/'+ idSeason + '/stage/' + idStage).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.listBySeasonStageCategory = function(idSeason, idStage, idCategory){
        return $http.get('/services/v1/subscription/season/'+ idSeason + '/stage/' + idStage + '/category/' + idCategory).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.save = function(vo){
        return $http.post('/services/v1/subscription/', vo).
        success(function(data, status) {
            AlertHelper.success("Salvo com sucesso!");
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.delete = function(id){
        return $http.post('/services/v1/subscription/delete/' + id).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    return factory;
}