'use strict';

_main.factory('ResultsService', ResultsService);
ResultsService.$inject = ['$http', 'AlertHelper'];
function ResultsService($http, AlertHelper) {

    var factory = {};

    factory.listBySeasonStageCategory = function(idSeason, idStage, idCategory){
        return $http.get('/services/v1/results/season/'+ idSeason + '/stage/' + idStage + '/category/' + idCategory).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.listBySeasonCategory = function(idSeason, idCategory){
        return $http.get('/services/v1/results/season/'+ idSeason + '/category/' + idCategory).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.listBySeasonStage = function(idSeason, idStage, idCategory){
        return $http.get('/services/v1/results/season/'+ idSeason + '/stage/' + idStage + '/category/' + idCategory + '/table').
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.listTeamBySeason = function(idSeason){
        return $http.get('/services/v1/results/team/season/'+ idSeason).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.save = function(list){
        return $http.post('/services/v1/results/', list).
        success(function(data, status) {
            AlertHelper.success("Salvo com sucesso!");
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    return factory;
}