'use strict';

_main.factory('AthleteService', AthleteService);
AthleteService.$inject = ['$http','AlertHelper'];
function AthleteService($http, AlertHelper) {

    var factory = {};
    factory.list = function(){
        return $http.get('/services/v1/athlete/').
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.listByName = function(name){
        return $http.get('/services/v1/athlete/' + name).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.save = function(vo){
        return $http.post('/services/v1/athlete/', vo).
        success(function(data, status) {
            AlertHelper.success("Salvo com sucesso!");
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.delete = function(id){
        return $http.post('/services/v1/athlete/delete/' + id).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    return factory;
}