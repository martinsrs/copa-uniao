'use strict';


_main.controller('AthleteController', AthleteController);
AthleteController.$inject = ['$scope', '$q', 'AthleteService'];
function AthleteController($scope, $q, AthleteService) {


    var _t = this;
    this.load = function () {

        $q.all({
            athletes : AthleteService.list()
        }).then(function(result) {
            _t.allSeasons = result.athletes.data;
        });

    }

    this.load();
}