'use strict';

_main.directive('athleteSearch', AthleteSearch);

function AthleteSearch() {

    return {
        templateUrl: 'scripts/admin/athlete/directive/AthleteSearch.html',
        restrict: 'E',
        scope : {
            ngModel : '='
        },
        controller: AthleteSearchCtrl,
        controllerAs : 'search',
        link: function (scope, elem, attr) {

        }
    };
}

AthleteSearchCtrl.$inject = ['$scope', '$rootScope', '$q', 'AthleteService'];

function AthleteSearchCtrl($scope, $rootScope, $q, AthleteService) {

    var _t = this;
    this.list = false;
    this.selectedAthlete = false;

    $scope.$watch('searchValue', function() {
        var query = $scope.searchValue;
        _t.list = false;
        _t.selectedAthlete = false;
        if (query != null && query.length >= 3) {
            $q.all({
                team : AthleteService.listByName(query)
            }).then(function(result) {
                _t.list = result.team.data;
            });
        }
    });

    this.clearSelection = function() {
        $scope.searchValue = "";
        _t.selectedAthlete = false;
        _t.list = false;
        $scope.ngModel = null;
        $rootScope.$broadcast('clear-athlete', {});
    };

    this.select = function(vo) {
        _t.selectedAthlete = true;
        _t.list = false;
        _t.athlete = vo;

        $scope.ngModel = vo;
    };

    $scope.$on('stage-changed', function (event) {
        _t.clearSelection();
    });
}