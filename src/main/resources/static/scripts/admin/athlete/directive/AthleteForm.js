'use strict';

_main.directive('athleteForm', AthleteForm);

function AthleteForm() {

    return {
        templateUrl: 'scripts/admin/athlete/directive/AthleteForm.html',
        restrict: 'E',
        scope : {
            modal : '='
        },
        controller   : AthleteFormCtrl,
        controllerAs : 'form',
        link: function (scope, elem, attr) {

        }
    };
}

AthleteFormCtrl.$inject = ['$scope', '$rootScope', '$q', 'AthleteService'];

function AthleteFormCtrl($scope, $rootScope, $q, AthleteService) {

    var _t = this;
    this.athlete = { team : null };

    this.disableSave = function() {
        if (_t.athlete != null) {
            if (_t.athlete.name != null
                && _t.athlete.city != null
                && _t.athlete.state != null
                && _t.athlete.team != null
                && _t.athlete.team.idTeam != null) {
                return false;
            }
        }
        return true;
    }

    this.save = function(vo) {

        $q.all({
            athlete : AthleteService.save(vo)
        }).then(function(result) {
            _t.athlete = { team : null };

            $rootScope.$broadcast('athlete-saved', {});
        });
    }
}