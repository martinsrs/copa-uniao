'use strict';

_main.directive('athleteTable', AthleteTable);

function AthleteTable() {

    return {
        templateUrl: 'scripts/admin/athlete/directive/AthleteTable.html',
        restrict: 'E',
        scope : {

        },
        controller : AthleteTableCtrl,
        controllerAs : 'table',
        link: function (scope, elem, attr) {

        }
    };
}

AthleteTableCtrl.$inject = ['$scope', '$rootScope', '$q', 'AthleteService'];

function AthleteTableCtrl($scope, $rootScope, $q, AthleteService) {
    var _t = this;

    this.load = function () {
        $q.all({
            team : AthleteService.list()
        }).then(function(result) {
            _t.list = result.team.data;
        });
    }

    this.turnEdit = function(vo) {
        if (vo.edit == null || vo.edit == false) {
            vo.edit = true;
        } else {
            vo.edit = false;
        }

        vo.team.idTeam = vo.team.idTeam.toString();
        return vo;
    }

    this.prepareDel = function(vo) {
        _t.toDeleteVo = vo;
    }

    this.save = function(vo) {
        $q.all({
            team : AthleteService.save(vo)
        }).then(function(result) {
            $rootScope.$broadcast('athlete-saved', {});
        });
    }

    this.delete = function(vo) {
        $q.all({
            team : AthleteService.delete(vo.idAthlete)
        }).then(function(result) {
            $rootScope.$broadcast('athlete-saved', {});
        });
    }

    this.load();

    $scope.$on('athlete-saved', function(event) {
        _t.load();
    });
}