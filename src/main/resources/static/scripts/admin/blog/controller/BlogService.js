'use strict';

_main.factory('BlogService', BlogService);
BlogService.$inject = ['$http', 'AppConfig', 'AlertHelper'];
function BlogService($http, AppConfig, AlertHelper) {

    var factory = {};
    factory.list = function(){
        return $http.get('/services/v1/blog/').
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.listPaginated = function(page){

        var limit = AppConfig.blogPageSize;

        return $http.get('/services/v1/blog/list?page='+ page +'&size=' + limit).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.getOne = function(id){
        return $http.get('/services/v1/blog/post/' + id).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.save = function(vo){
        return $http.post('/services/v1/blog/', vo).
        success(function(data, status) {
            AlertHelper.success("Postagem salva com sucesso!");
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.delete = function(id){
        return $http.post('/services/v1/blog/delete/' + id).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    return factory;
}