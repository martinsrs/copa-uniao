'use strict';

_main.controller('BlogController', BlogController);
BlogController.$inject = ['$scope', '$rootScope', '$routeParams', '$q', 'BlogService', 'LoginService'];
function BlogController($scope, $rootScope, $routeParams, $q, BlogService, LoginService) {

    var _t = this;

    this.load = function (id) {

        $q.all({
            getOne : BlogService.getOne(id)
        }).then(function(result) {
            _t.post = result.getOne.data;
        });

    }

    this.save = function (post) {
        var user = LoginService.getUserSession();
        post.author = user;

        $q.all({
            save : BlogService.save(post)
        }).then(function(result) {
            _t.post = result.save.data;
            $rootScope.$broadcast('post-saved', _t.post.idPost);
        });
    }

    $scope.$on('post-saved', function(event,val) {
        if (val != null) {
            _t.load(val);
        }
    });

    if ($routeParams != null) {
        if ($routeParams.idPost != null) {
            var idPost = $routeParams.idPost;
            _t.load(idPost);
        }
    }

}
