'use strict';

_main.directive('postTable', PostTable);

function PostTable() {

    return {
        templateUrl: 'scripts/admin/blog/directive/PostTable.html',
        restrict: 'E',
        scope : {

        },
        controller : PostTableCtrl,
        controllerAs : 'table',
        link: function (scope, elem, attr) {

        }
    };
}

PostTableCtrl.$inject = ['$scope', '$rootScope', '$q', 'BlogService'];

function PostTableCtrl($scope, $rootScope, $q, BlogService) {
    var _t = this;

    this.load = function () {
        $q.all({
            team : BlogService.list()
        }).then(function(result) {
            _t.list = result.team.data;
        });
    }

    this.setPublish = function (vo, flag) {

        vo.publish = flag;
        _t.save(vo);

    }

    this.prepareDel = function(vo) {
        _t.toDeleteVo = vo;
    }

    this.save = function(vo) {
        $q.all({
            post : BlogService.save(vo)
        }).then(function(result) {
            var post = result.post.data;
            $rootScope.$broadcast('post-saved', post.idPost);
        });
    }

    this.delete = function(vo) {
        $q.all({
            post : BlogService.delete(vo.idPost)
        }).then(function(result) {
            $rootScope.$broadcast('post-saved', null);
        });
    }

    this.load();

    $scope.$on('post-saved', function(event) {
        _t.load();
    });
}