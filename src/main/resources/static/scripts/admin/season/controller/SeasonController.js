'use strict';

/**
 * @ngdoc function
 * @name cucSite.controller:SeasonController
 * @description
 * # SeasonController
 * Controller of the cucSite
 */
_main.controller('SeasonController', SeasonController);

SeasonController.$inject = ['$scope', '$rootScope', '$route', '$window', '$q', 'SeasonService'];
function SeasonController($scope, $rootScope, $route, $window, $q, SeasonService) {


    var _t = this;

    this.seasonVo = {
        year : '',
        edition : ''
    }

    this.loadSeasons = function () {
        $q.all({
            seasons: SeasonService.list()
        }).then(function(result) {
            _t.allSeasons = result.seasons.data;
        });
    }

    this.save = function(vo) {

        vo.active = false;
        $q.all({
            seasons: SeasonService.save(vo)
        }).then(function (result) {
            $scope.$broadcast('season-saved', {});
        });

    }

    this.disableSave = function () {

        if (_t.seasonVo.year == '' || _t.seasonVo.edition == '') {
            return true;
        }

        return false;
    }


    this.loadSeasons();
}