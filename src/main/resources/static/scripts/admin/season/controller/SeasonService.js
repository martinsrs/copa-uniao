'use strict';


_main.factory('SeasonService', SeasonService);

SeasonService.$inject = ['$http', 'AlertHelper'];
function SeasonService($http, AlertHelper) {

    var factory = {};
    factory.list = function(){
        return $http.get('/services/v1/season/').
                success(function(data, status) {
                    return data;
                }).
                error(function(data, status) {
                    AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
                });
    };

    factory.listActive = function(){
        return $http.get('/services/v1/season/active').
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.get = function(idSeason){
        return $http.get('/services/v1/season/' + idSeason).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.save = function(vo){
        return $http.post('/services/v1/season/', vo).
        success(function(data, status) {
            AlertHelper.success("Salvo com sucesso!");
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.delete = function(idStage){
        return $http.post('/services/v1/season/delete/' + idStage).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };


    factory.assignCategories = function(idSeason, categories){
        return $http.post('/services/v1/season/' + idSeason + '/category/', categories).
        success(function(data, status) {
            AlertHelper.success("Categorias adicionadas com sucesso!");
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    return factory;
}