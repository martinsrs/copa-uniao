'use strict';

/**
 * @ngdoc directive
 * @name cucSite.directive:CopyCategotyCombo
 * @description
 * # CopyCategotyCombo
 */
_main.directive('copyCategoryCombo', CopyCategotyCombo);

function CopyCategotyCombo() {

    return {
        template:   '<select class="form-control" ng-model="ngModel">' +
                        '<option value="" selected>Não Copiar</option>' +
                        '<option ng-repeat="vo in combo.list" value="{{vo.idSeason}}">{{vo.year}} - {{vo.edition}}</option>' +
                    '</select>',
        restrict: 'E',
        scope : {
            ngModel : '='
        },
        controller    : CopyCategotyComboCtrl,
        controllerAs  : 'combo',
        link: function (scope, elem, attr) {
        }
    };
}

CopyCategotyComboCtrl.$inject = ['$scope', '$rootScope', '$q', 'SeasonService'];
function CopyCategotyComboCtrl($scope, $rootScope, $q, SeasonService) {

    var _t = this;

    this.reload = function() {
        $q.all({
            seasons: SeasonService.list()
        }).then(function(result) {
            _t.list = result.seasons.data;
        });
    }

    $scope.$on('season-saved', function (event) {
        _t.reload();
    });

    this.reload();
}