'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:SelectCategory
 * @description
 * # SelectCategory
 */
_main.directive('selectCategory', SelectCategory);

function SelectCategory() {

    return {
        templateUrl: 'scripts/admin/season/directive/SelectCategory.html',
        restrict: 'E',
        scope : {

            season : '='

        },
        controller  : SelectCategoryCtrl,
        controllerAs : 'ctrl',
        link: function (scope, elem, attr) {
        }
    };
}

SelectCategoryCtrl.$inject = ['$scope', '$rootScope', '$q', 'CategoryService', 'SeasonService'];
function SelectCategoryCtrl($scope, $rootScope, $q, CategoryService, SeasonService) {

    var _t = this;
    
    $scope.$watch('season', function() {
        _t.selectedCategories = [];
        if ($scope.season != null && $scope.season.idSeason != null) {
            angular.forEach(_t.list, function (valueAll, key) {
                angular.forEach($scope.season.categories, function (valueSeason, key1) {
                    if (valueAll.idCategory == valueSeason.idCategory) {
                        _t.selectedCategories[valueAll.idCategory] = true;
                    }
                });
            });
        }
    });

    this.load = function () {
        $q.all({
            categories : CategoryService.list()
        }).then(function(result) {
            _t.list = result.categories.data;
        });
    }

    this.save = function() {

        var selectedList = [];
        angular.forEach(_t.selectedCategories, function (value, key) {
            if (value == true || value == 1) {

                var category = {
                    idCategory : key
                };
                selectedList.push(category);
            }
        });

        $q.all({
            season : SeasonService.assignCategories($scope.season.idSeason, selectedList)
        }).then(function(result) {
            $rootScope.$broadcast('season-saved', {});
        });

    }

    this.load();

}