'use strict';

/**
 * @ngdoc directive
 * @name cucSite.directive:SeasonCombo
 * @description
 * # SeasonCombo
 */
_main.directive('seasonCombo', SeasonCombo);

function SeasonCombo() {
    return {
        template: '<select class="form-control" ng-model="ngModel" ng-disabled="ngDisabled">' +
            '<option value="" disabled selected>Selecione uma temporada...</option>' +
            '<option ng-repeat="season in combo.list" value="{{season.idSeason}}">{{season.year}} - {{season.edition}}</option>' +
            '</select>',
        restrict: 'E',
        scope : {
            ngModel : '=',
            ngDisabled : '='
        },
        controller: SeasonComboCtrl,
        controllerAs : 'combo',
        link: function (scope, elem, attr) {

        }
    };
}

SeasonComboCtrl.$inject = ['$scope', '$q', 'SeasonService'];
function SeasonComboCtrl($scope, $q, SeasonService) {

    var _t = this;

    this.reload = function() {
        $q.all({
            seasons: SeasonService.list()
        }).then(function(result) {
            _t.list = result.seasons.data;
        });
    }

    $scope.$on('season-saved', function (event) {
        _t.reload();
    });

    this.reload();

}