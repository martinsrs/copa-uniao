'use strict';

/**
 * @ngdoc directive
 * @name cucSite.directive:SeasonTable
 * @description
 * # SeasonTable
 */
_main.directive('seasonTable', SeasonTable);

function SeasonTable() {

    return {
        templateUrl: 'scripts/admin/season/directive/SeasonTable.html',
        restrict: 'E',
        scope : {

        },
        controller    : SeasonTableCtrl,
        controllerAs  : 'table',
        link: function (scope, elem, attr) {

        }
    };
}

SeasonTableCtrl.$inject = ['$scope', '$rootScope', '$q', 'SeasonService'];
function SeasonTableCtrl($scope, $rootScope, $q, SeasonService) {

    var _t = this;

    this.reload = function () {
        $q.all({
            seasons: SeasonService.list()
        }).then(function(result) {
            _t.seasonAllList = result.seasons.data;
        });
    }

    this.turnEdit = function(vo) {
        if (vo.edit == null || vo.edit == false) {
            vo.edit = true;
        } else {
            vo.edit = false;
        }
        return vo;
    }

    this.save = function(vo) {

        $q.all({
            seasons: SeasonService.save(vo)
        }).then(function(result) {
            $rootScope.$broadcast('season-saved', {});
        });

    }

    this.popUp = function(vo) {
        _t.popUpVo = vo;
    }

    this.prepareDel = function(vo) {
        _t.toDeleteVo = vo;
    }

    this.delete = function(vo) {
        $q.all({
            seasons: SeasonService.delete(vo.idSeason)
        }).then(function(result) {
            $rootScope.$broadcast('season-saved', {});
        });

    }

    this.setPublish = function (vo, flag) {
        vo.active = flag;
        this.save(vo);
    }

    $scope.$on('season-saved', function (event) {
        _t.reload();
    });

    this.reload();
}