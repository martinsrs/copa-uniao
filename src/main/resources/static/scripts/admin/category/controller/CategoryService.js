'use strict';


_main.factory('CategoryService', CategoryService);

CategoryService.$inject = ['$http', 'AlertHelper'];
function CategoryService($http, AlertHelper) {

    var factory = {};
    factory.list = function(){
        return $http.get('/services/v1/category/').
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.listBySeason = function(id){
        return $http.get('/services/v1/category/season/' + id).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.get = function(id){
        return $http.get('/services/v1/category/' + id).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.save = function(vo){
        return $http.post('/services/v1/category/', vo).
        success(function(data, status) {
            AlertHelper.success("Salvo com sucesso!");
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };


    factory.delete = function(idCategory){
        return $http.post('/services/v1/category/delete/' + idCategory, {}).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    return factory;
}