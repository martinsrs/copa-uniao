'use strict';

/**
 * @ngdoc function
 * @name cucSite.controller:CategoryController
 * @description
 * # CategoryController
 * Controller of the cucSite
 */
_main.controller('CategoryController', CategoryController);

CategoryController.$inject = ['$scope', '$rootScope', '$route', '$window', '$q', 'CategoryService'];
function CategoryController($scope, $rootScope, $route, $window, $q, CategoryService) {


    var _t = this;

    this.categoryVo = {
        name : '',
        criteria : '',
        subscription : ''
    }

    this.load = function () {
        $q.all({
            categories : CategoryService.list()
        }).then(function(result) {
            _t.allSeasons = result.categories.data;
        });
    }

    this.save = function(vo) {

        vo.active = false;
        $q.all({
            category : CategoryService.save(vo)
        }).then(function (result) {
            $scope.$broadcast('category-saved', {});
        });

    }

    this.disableSave = function () {

        if (_t.categoryVo.name == '' || _t.categoryVo.criteria == '') {
            return true;
        }

        return false;
    }


    this.load();
}