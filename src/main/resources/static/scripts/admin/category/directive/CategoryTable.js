'use strict';

_main.directive('categoryTable', CategoryTable);
function CategoryTable() {

    return {
        templateUrl: 'scripts/admin/category/directive/CategoryTable.html',
        restrict: 'E',
        scope : {

        },
        controller    : CategoryTableCtrl,
        controllerAs  : 'table',
        link: function (scope, elem, attr) {

        }
    };
}

CategoryTableCtrl.$inject = ['$scope', '$rootScope', '$q', 'CategoryService'];
function CategoryTableCtrl($scope, $rootScope, $q, CategoryService) {

    var _t = this;

    this.reload = function () {
        $q.all({
            categories : CategoryService.list()
        }).then(function(result) {
            _t.listAll = result.categories.data;
        });
    }

    this.turnEdit = function(vo) {
        if (vo.edit == null || vo.edit == false) {
            vo.edit = true;
        } else {
            vo.edit = false;
        }
        return vo;
    }

    this.save = function(vo) {

        $q.all({
            category : CategoryService.save(vo)
        }).then(function(result) {
            $rootScope.$broadcast('category-saved', {});
        });

    }

    this.popUp = function(vo) {
        _t.popUpVo = vo;
    }

    this.prepareDel = function(vo) {
        _t.toDeleteVo = vo;
    }

    this.delete = function(vo) {
        $q.all({
            categories : CategoryService.delete(vo.idCategory)
        }).then(function(result) {
            $rootScope.$broadcast('category-saved', {});
        });

    }

    $scope.$on('category-saved', function (event) {
        _t.reload();
    });

    this.reload();
}