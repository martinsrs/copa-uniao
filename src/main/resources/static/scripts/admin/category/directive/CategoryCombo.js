'use strict';


_main.directive('categoryCombo', CategoryCombo);
function CategoryCombo() {
    return {
        template: '<select class="form-control" ng-model="ngModel">' +
            '<option value="" disabled>Selecione uma Categoria...</option>' +
            '<option ng-repeat="category in combo.list" value="{{category.idCategory}}">{{category.name}}</option>' +
            '</select>',
        restrict: 'E',
        scope : {
            season  : '=',
            ngModel : '='
        },
        controller: CategoryComboCtrl,
        controllerAs : 'combo',
        link: function (scope, elem, attr) {

        }
    };
}

CategoryComboCtrl.$inject = ['$scope', '$q', 'CategoryService'];
function CategoryComboCtrl($scope, $q, CategoryService ) {

    var _t = this;
    this.load = function () {

        if ($scope.season != null) {
            $q.all({
                category : CategoryService.listBySeason($scope.season)
            }).then(function(result) {
                _t.list = result.category.data;
            });
        } else {
            $q.all({
                category : CategoryService.list()
            }).then(function(result) {
                _t.list = result.category.data;
            });
        }
    }

    this.load();

    $scope.$on('category-saved', function(event) {
        _t.load();
    });

    $scope.$watch('season', function(event) {
        _t.load();
    });
}