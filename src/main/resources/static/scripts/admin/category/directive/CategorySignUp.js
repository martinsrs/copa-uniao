'use strict';

_main.directive('categorySignUp', CategorySignUp);

function CategorySignUp() {

    return {
        templateUrl: 'scripts/admin/category/directive/CategorySignUp.html',
        restrict: 'E',
        scope : {
            ngModel : '=',
            season  : '=',
            stage   : '=',
            athlete : '='
        },
        controller: CategorySignUpCtrl,
        controllerAs : 'signup',
        link: function (scope, elem, attr) {

        }
    };
}

CategorySignUpCtrl.$inject = ['$scope', '$q', 'CategoryService', 'SubscriptionService'];

function CategorySignUpCtrl($scope, $q, CategoryService, SubscriptionService) {

    var _t = this;

    this.load = function () {

        var idAthlete = $scope.athlete.idAthlete;
        var idSeason  = $scope.season;
        var idStage   = $scope.stage;

        _t.selectedCategory = null;

        $q.all({
            subscriptions : SubscriptionService.listByAthleteSeasonStage(idAthlete, idSeason, idStage)
        }).then(function(result) {
            if ($scope.ngModel == null) {
                $scope.ngModel = [];
            }
            var list = result.subscriptions.data;

            if (list.length > 0) {
                list.forEach(function(item) {
                    var category = item.category;
                    category.number = item.number;
                    $scope.ngModel.push(category);
                });
            } else {
                list = null;
                $scope.ngModel = list;
            }
        });
    }

    this.add = function(id) {
        var exists = false;

        if (id != null) {
            angular.forEach($scope.ngModel, function (value, key) {
                if (value.idCategory == id) {
                    exists = true;
                }
            });

            if (!exists) {
                $q.all({
                    category: CategoryService.get(id)
                }).then(function (result) {
                    if ($scope.ngModel == null) {
                        $scope.ngModel = [];
                    }
                    $scope.ngModel.push(result.category.data);
                    _t.selectedCategory = null;
                });
            }
        }

    }

    this.remove = function(vo) {

        var idToRemove = vo.idCategory;
        angular.forEach($scope.ngModel, function (value, key) {
            if (value.idCategory == idToRemove) {
                $scope.ngModel.splice(key, 1);
            }
        });

        if ($scope.ngModel.length == 0) {
            $scope.ngModel = null;
        }
    }

    this.clear = function() {
        $scope.ngModel = null;
    }

    $scope.$on('clear-athlete', function (event) {
        _t.clear();
    });

    $scope.$watch('athlete', function (newVal) {
        if ($scope.athlete != null) {
            _t.load();
        }
    });

}