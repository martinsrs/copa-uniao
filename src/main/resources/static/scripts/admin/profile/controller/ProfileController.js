'use strict';

_main.controller('ProfileController', ProfileController);
ProfileController.$inject = ['$scope', '$q', 'ProfileService', 'LoginService', 'AlertHelper'];
function ProfileController($scope, $q, ProfileService, LoginService, AlertHelper) {

    
    var _t = this;
    
    this.getProfile = function () {
        var user = LoginService.getUserSession();
        $q.all({
            profile : ProfileService.get(user.idUser)
        }).then(function(result) {
            _t.user = result.profile.data;
        });
    }

    this.save = function (user) {

        if (user.tmpPass != null && user.tmpPass == user.tmpPass2) {
            user.password = user.tmpPass;
            $q.all({
                profile : ProfileService.save(user)
            }).then(function(result) {
                LoginService.authenticate(result.profile.data);
                _t.getProfile();
            });
        } else {
            AlertHelper.error('As senhas informadas não conferem!');
        }

    }

    this.getProfile();
}
