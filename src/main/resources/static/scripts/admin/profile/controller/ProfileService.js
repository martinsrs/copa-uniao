'use strict';

_main.factory('ProfileService', ProfileService);
ProfileService.$inject = ['$http', '$cookieStore', 'AppConfig', 'AlertHelper'];
function ProfileService($http, $cookieStore, AppConfig, AlertHelper) {

    var factory = {};
    factory.get = function(id){

        return $http.get('/services/v1/profile/' + id).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.list = function(){

        return $http.get('/services/v1/profile/').
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.save = function(user){

        return $http.post('/services/v1/profile/', user).
        success(function(data, status) {
            AlertHelper.success("Salvo com sucesso!");
            return data;
        }).
        error(function(data, status) {

            if (status == 409) {
                AlertHelper.error("OPS! Email já cadastrado!");
            } else {
                AlertHelper.error("OPS! Ocorreu um erro!");
            }
            console.error(status);
        });
    };

    factory.delete = function(id){

        return $http.post('/services/v1/profile/delete/' + id, {}).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    return factory;
}