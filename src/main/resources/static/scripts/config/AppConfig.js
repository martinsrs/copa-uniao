'use strict';

_main.factory('AppConfig', AppConfig);
function AppConfig() {
    return {
        USER_SESSION_KEY: 'loggedUser',
        blogPageSize: 5,
        previewPostLength: 220,
        dateTimeFix: 'T19:43:37+0100',
        dateLocale: 'pt-BR',
        dateFormat: {
            default: 'YYYY-MM-DD',
            long: 'dddd, DD [de] MMMM [de] YYYY',
            short: 'DD/MM/YYYY'
        }
    }
}