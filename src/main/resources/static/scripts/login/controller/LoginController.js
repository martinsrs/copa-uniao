'use strict';

_main.controller('LoginController', LoginController);
LoginController.$inject = ['$scope', '$rootScope', '$q', '$location', 'LoginService'];
function LoginController($scope, $rootScope, $q, $location, LoginService) {

    var _t = this;
    _t.loginError = false;

    this.isUserAuthenticated = function () {
        return LoginService.getUserAuthenticated();
    }

    this.login = function (user) {

        _t.loginError = false;
        $q.all({
            user : LoginService.login(user)
        }).then(function(result) {
            LoginService.authenticate(result.user.data);
            $rootScope.$broadcast('login', {});
            $location.path('/');

        }).catch(function (error) {
            if (error.status == 401) {
                _t.loginError = true;
            }
        });
    }

    this.logout = function () {
        LoginService.logout();
        $rootScope.$broadcast('logout', {});
    }

    this.cancel = function () {
        $location.path('/');
    }

}
