'use strict';

_main.factory('LoginService', LoginService);
LoginService.$inject = ['$http', '$cookieStore', 'AppConfig', 'AlertHelper'];
function LoginService($http, $cookieStore, AppConfig, AlertHelper) {

    var factory = {};
    factory.login = function(user){

        return $http.post('/services/v1/login/', user).
        success(function(data, status) {
            return data;
        }).
        error(function(data, status) {
            AlertHelper.error("OPS! Ocorreu um erro!");
            console.error(status);
        });
    };

    factory.authenticate = function(user) {
        $cookieStore.put(AppConfig.USER_SESSION_KEY, user);
    };

    factory.logout = function() {
        $cookieStore.remove(AppConfig.USER_SESSION_KEY);
    };

    factory.getUserSession = function() {
        var user = $cookieStore.get(AppConfig.USER_SESSION_KEY);
        return user;
    };

    factory.getUserAuthenticated = function() {
        var authenticated = false;
        var user = $cookieStore.get(AppConfig.USER_SESSION_KEY);

        if (user != null) {
            authenticated = true;
        }
        return authenticated;
    };

    return factory;
}